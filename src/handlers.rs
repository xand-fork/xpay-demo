use crate::backend::domain_state::{DomainUiRecipient, IState};
use crate::{
    Address, AppWindow, ComponentHandle, CreatePageHandlers, DigitGroupingPolicy, DigitGroupingsExt, DomainStateHolder,
    PaymentPageHandlers, RedeemPageHandlers, StateHandlers, UiBankAccount, UiHandlerProvider, UiMemberData,
    UiRecipient, Usd,
};
use slint::{Model, ModelRc, SharedString};

pub fn assign(app_window: &AppWindow, state_container: &DomainStateHolder, handler_provider: &UiHandlerProvider) {
    // Pay handler
    let state_handle = state_container.clone();
    let provider = handler_provider.clone();
    let payment_handlers = app_window.global::<PaymentPageHandlers>();
    payment_handlers.on_make_payment(move |recipient: UiRecipient, amount| {
        let state = state_handle.state.as_ref().borrow();
        let current_member = state.get_current_member();
        let current_bank_account = state.get_current_bank_account().unwrap();
        drop(state);

        let recipient = Address(recipient.address.to_string());
        dbg!(&recipient);
        let amount = amount.replace(',', "");
        let amount = Usd::try_from_str(amount).unwrap();
        let ui_handler = provider.new_ui_handler_for(current_member);
        ui_handler.pay(&current_bank_account, &recipient, amount, state_handle.clone())
    });

    // Redeem handler
    let state_handle = state_container.clone();
    let provider = handler_provider.clone();
    let redeem_handlers = app_window.global::<RedeemPageHandlers>();
    redeem_handlers.on_redeem_digital_cash(move |_account_index, amount| {
        let state = state_handle.state.as_ref().borrow();
        let current_member = state.get_current_member();
        let current_bank_account = state.get_current_bank_account().unwrap();
        drop(state);

        let amount = amount.replace(',', "");
        let amount = Usd::try_from_str(amount).unwrap();
        let ui_handler = provider.new_ui_handler_for(current_member);
        ui_handler.redeem_funds(&current_bank_account, amount, state_handle.clone())
    });

    // Create handler
    let state_handle = state_container.clone();
    let provider = handler_provider.clone();
    let create_handlers = app_window.global::<CreatePageHandlers>();
    create_handlers.on_create(move |amount| {
        let state = state_handle.state.as_ref().borrow();
        let current_member = state.get_current_member();
        let current_bank_account = state.get_current_bank_account().unwrap();
        drop(state);

        let amount = Usd::try_from_str(amount).unwrap();
        let ui_handler = provider.new_ui_handler_for(current_member);
        ui_handler.create(&current_bank_account, amount, state_handle.clone());
    });

    // Display handlers
    // Set Member handler
    let ui_state_handlers = app_window.global::<StateHandlers>();

    let state_handle = state_container.clone();
    let provider = handler_provider.clone();
    ui_state_handlers.on_set_member(move |_m_data| {
        let state = state_handle.state.as_ref().borrow();
        state.ui_state.clear_bank_balance();
        state.ui_state.clear_xand_balance();

        let new_member = state.get_current_member();

        // TODO: Clear out possible_bank_accounts and fetch for new ones.
        let current_account = state.get_current_bank_account().unwrap();
        drop(state);

        let ui_handler = provider.new_ui_handler_for(new_member);
        ui_handler.update_balances(&current_account, state_handle.clone());
        // ui_handler.update_xand_balance(state_handle.clone());
    });

    let state_handle = state_container.clone();
    ui_state_handlers.on_current_member_name(move || {
        let state = state_handle.state.as_ref().borrow();
        state.get_current_member_name().into()
    });

    ui_state_handlers.on_set_bank_account(|_ui_bank_account| {
        // TODO: Clear out relevant state and kick off fetching new balance
        todo!();
    });

    ui_state_handlers.on_format_usd(|amt| match Usd::try_from_str(amt.clone()) {
        Ok(usd) => {
            let usd_str = usd.to_major_units().group_digits(DigitGroupingPolicy::Common);
            usd_str.into()
        },
        Err(_err) => amt,
    });

    ui_state_handlers.on_display_members(|ui_member_data| {
        let ui_member_data_vec: Vec<UiMemberData> = ui_member_data.iter().collect();
        let addresses: Vec<SharedString> = ui_member_data_vec.iter().map(|ui_m| ui_m.address.clone()).collect();
        slint::ModelRc::new(slint::VecModel::from(addresses))
    });

    ui_state_handlers.on_display_bank_accounts(|ui_bank_data| {
        let ui_bank_data_vec: Vec<UiBankAccount> = ui_bank_data.iter().collect();
        let addresses: Vec<SharedString> = ui_bank_data_vec.iter().map(|ui_m| ui_m.account.clone()).collect();
        slint::ModelRc::new(slint::VecModel::from(addresses))
    });

    let state_handle = state_container.clone();
    ui_state_handlers.on_get_recipients(move || {
        let state = state_handle.state.as_ref().borrow();
        let recs: Vec<DomainUiRecipient> = state.possible_recipients();

        let recs: Vec<UiRecipient> = recs.into_iter().map(Into::into).collect();
        slint::ModelRc::new(slint::VecModel::from(recs))
    });

    ui_state_handlers.on_get_recipients_strs(move |recipients: ModelRc<UiRecipient>| {
        let friendly_recipient_names: Vec<SharedString> = recipients.iter().map(|r| r.friendly_name).collect();
        slint::ModelRc::new(slint::VecModel::from(friendly_recipient_names))
    });

    let state_handle = state_container.clone();
    ui_state_handlers.on_set_network_spec(move |network_spec: SharedString| {
        println!("set network spec called");
        let mut state = state_handle.state.as_ref().borrow_mut();
        match state.set_network_spec(network_spec.to_string()) {
            Ok(_) => true,
            Err(err) => {
                println!("Error occurred setting network spec: {}", err);
                false
            },
        }
    });
}
