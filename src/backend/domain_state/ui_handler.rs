use crate::backend::domain_state::create_promise::CreatePromise;
use crate::backend::domain_state::data::MemberData;
use crate::backend::domain_state::payment_promise::PaymentPromise;
use crate::backend::domain_state::redeem_promise::RedeemPromise;
use crate::{
    backend::{
        clients::member_api_client::MemberApiClient,
        domain_state::{
            api_doer::ApiDoer,
            data::{Account, Address, Usd},
        },
        runtime_adapter::spawn_async,
    },
    DomainStateHolder,
};
use std::sync::mpsc::channel;
use std::sync::{mpsc::Sender, Arc};

#[derive(Clone)]
pub struct UiHandlerProvider {
    sender: Sender<FollowupAction>,
}

impl UiHandlerProvider {
    pub fn new(sender: Sender<FollowupAction>) -> Self {
        UiHandlerProvider { sender }
    }

    pub fn new_ui_handler_for(&self, m: MemberData) -> UiHandler {
        let url = m.member_api_details.member_api_url;
        let token: String = m.member_api_details.auth.map_or_else(|| "".into(), |jwt_info| jwt_info.token.0);
        let client = MemberApiClient::new(url, token);
        let api_doer = Arc::new(ApiDoer::new(client));
        UiHandler { api_doer, sender: self.sender.clone() }
    }
}

#[derive(Clone)]
pub struct UiHandler {
    api_doer: Arc<ApiDoer<MemberApiClient>>,
    sender: Sender<FollowupAction>,
}

#[derive(Debug)]
pub enum FollowupAction {
    RefreshBalances,
}

impl UiHandler {
    pub fn update_balances(&self, account: &Account, state: DomainStateHolder) {
        self.update_bank_balance(account, state.clone());
        self.update_xand_balance(state);
    }

    fn update_bank_balance(&self, account: &Account, state: DomainStateHolder) {
        let api_doer = self.api_doer.clone();
        let id = account.id();

        let bal_prom = spawn_async(async move {
            let bal_fut = api_doer.get_account_balance(id);
            bal_fut.await
        });
        let mut state = state.state.as_ref().borrow_mut();
        state.promises.bank_bal_promise = Some(bal_prom);
    }

    pub fn update_xand_balance(&self, state: DomainStateHolder) {
        let api_doer = self.api_doer.clone();
        let bal_prom = spawn_async(async move {
            let bal_fut = api_doer.get_xand_balance();
            bal_fut.await
        });
        let mut state = state.state.as_ref().borrow_mut();
        state.promises.xand_bal_promise = Some(bal_prom);
    }

    pub fn create(&self, bank_account: &Account, amount: Usd, state: DomainStateHolder) {
        let api_doer = self.api_doer.clone();
        let bank_account = bank_account.clone();

        let (updates_sender, updates_receiver) = channel();
        let inner_promise = spawn_async(async move { api_doer.create(&bank_account, amount, updates_sender).await });
        let mut state = state.state.as_ref().borrow_mut();
        state.promises.create_promise = Some(CreatePromise::new(inner_promise, updates_receiver));
    }

    pub fn pay(&self, bank_account: &Account, recipient: &Address, amount: Usd, state: DomainStateHolder) {
        let api_doer = self.api_doer.clone();
        let bank_account = bank_account.clone();
        let recipient = recipient.clone();

        let sender_handle = self.sender.clone();
        let (updates_sender, updates_receiver) = channel();
        let inner_promise = spawn_async(async move {
            let receipt = api_doer.pay(&bank_account, &recipient, amount, updates_sender).await;
            if let Err(e) = sender_handle.send(FollowupAction::RefreshBalances) {
                dbg!(e);
            }
            receipt
        });
        let mut state = state.state.as_ref().borrow_mut();
        let payment_promise = PaymentPromise::new(inner_promise, updates_receiver);
        state.promises.payment_promise = Some(payment_promise);
    }

    pub fn redeem_funds(&self, bank_account: &Account, amount: Usd, state: DomainStateHolder) {
        let api_doer = self.api_doer.clone();
        let bank_account = bank_account.clone();

        let sender_handle = self.sender.clone();
        let (updates_sender, updates_receiver) = channel();
        let inner_promise = spawn_async(async move {
            let receipt = api_doer.redeem(&bank_account, amount, updates_sender).await;
            if let Err(e) = sender_handle.send(FollowupAction::RefreshBalances) {
                dbg!(e);
            }
            receipt
        });
        let mut state = state.state.as_ref().borrow_mut();
        let redeem_promise = RedeemPromise::new(inner_promise, updates_receiver);
        state.promises.redeem_promise = Some(redeem_promise);
    }
}
