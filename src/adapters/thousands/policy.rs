use crate::DigitGroupingPolicy;
use thousands::{policies, SeparatorPolicy as ThousandsSeparatorPolicy};

impl<'a> From<DigitGroupingPolicy> for ThousandsSeparatorPolicy<'a> {
    fn from(policy: DigitGroupingPolicy) -> Self {
        use DigitGroupingPolicy::*;
        match policy {
            Common => policies::COMMA_SEPARATOR,
        }
    }
}
