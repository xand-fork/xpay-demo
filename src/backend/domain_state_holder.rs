use crate::DomainState;
use std::{cell::RefCell, sync::Arc};

#[derive(Clone)]
pub struct DomainStateHolder {
    pub state: Arc<RefCell<DomainState>>,
}

impl DomainStateHolder {
    pub fn new(state: DomainState) -> Self {
        Self { state: Arc::new(RefCell::new(state)) }
    }
}
