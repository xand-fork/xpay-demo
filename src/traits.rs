mod digit_grouping;

pub use digit_grouping::DigitGroupingPolicy;

pub trait DigitGroupingsExt: Copy {
    fn group_digits(self, policy: DigitGroupingPolicy) -> String;
}
