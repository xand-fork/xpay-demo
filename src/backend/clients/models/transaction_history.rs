// Member API
//
// Member API routes and contracts.
//
// The version of the OpenAPI document: 1.0.0
//
// Generated by: https://openapi-generator.tech

#[derive(Clone, Debug, PartialEq, Default, Serialize, Deserialize)]
pub struct TransactionHistory {
    #[serde(rename = "total", skip_serializing_if = "Option::is_none")]
    pub total: Option<i32>,
    #[serde(rename = "transactions", skip_serializing_if = "Option::is_none")]
    pub transactions: Option<Vec<crate::backend::clients::models::Transaction>>,
}

impl TransactionHistory {
    pub fn new() -> TransactionHistory {
        TransactionHistory { total: None, transactions: None }
    }
}
