// Member API
//
// Member API routes and contracts.
//
// The version of the OpenAPI document: 1.0.0
//
// Generated by: https://openapi-generator.tech

#[derive(Clone, Debug, PartialEq, Default, Serialize, Deserialize)]
pub struct BankAccountBalance {
    #[serde(rename = "accountId")]
    pub account_id: String,
    #[serde(rename = "currentBalanceInMinorUnit")]
    pub current_balance_in_minor_unit: i32,
    #[serde(rename = "availableBalanceInMinorUnit")]
    pub available_balance_in_minor_unit: i32,
}

impl BankAccountBalance {
    pub fn new(
        account_id: String,
        current_balance_in_minor_unit: i32,
        available_balance_in_minor_unit: i32,
    ) -> BankAccountBalance {
        BankAccountBalance { account_id, current_balance_in_minor_unit, available_balance_in_minor_unit }
    }
}
