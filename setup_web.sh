#!/bin/bash
set -eu

# Pre-requisites:
rustup target add wasm32-unknown-unknown
cargo install wasm-pack --version 0.10.2

cargo install wasm-bindgen-cli

#apt-get install binaryen
#cargo update -p wasm-bindgen

# For local tests with `./start_server`:
#cargo install basic-http-server
