// Member API
//
// Member API routes and contracts.
//
// The version of the OpenAPI document: 1.0.0
//
// Generated by: https://openapi-generator.tech

#[derive(Clone, Debug, PartialEq, Default, Serialize, Deserialize)]
pub struct UpdateBank {
    #[serde(rename = "name")]
    pub name: String,
    #[serde(rename = "routingNumber")]
    pub routing_number: String,
    #[serde(rename = "trustAccount")]
    pub trust_account: String,
    #[serde(rename = "adapter")]
    pub adapter: Box<crate::backend::clients::models::BankAdapter>,
}

impl UpdateBank {
    pub fn new(
        name: String,
        routing_number: String,
        trust_account: String,
        adapter: crate::backend::clients::models::BankAdapter,
    ) -> UpdateBank {
        UpdateBank { name, routing_number, trust_account, adapter: Box::new(adapter) }
    }
}
