use super::*;
use crate::backend::{
    clients::{
        member_api_client::{error::Error, GetTransaction},
        models::Transaction,
    },
    domain_state::data::{BookTransferReceipt, TransactionReceipt, Usd},
};
use std::sync::mpsc::channel;
use std::{
    cell::RefCell,
    sync::{Arc, Mutex},
};

#[derive(Clone)]
pub struct MockMemberApi {
    pub bank_balance_return: Usd,
    pub xand_balance_return: Usd,
    pub get_txn_returns: Arc<Mutex<RefCell<Box<dyn Iterator<Item = GetTransaction> + Send>>>>,
    pub api_calls: Arc<Mutex<RefCell<Vec<ApiCall>>>>,
}

#[derive(Debug, PartialEq, Eq)]
pub enum ApiCall {
    Query(ApiQuery),
    Transact(ApiTransact),
}

impl From<ApiTransact> for ApiCall {
    fn from(t: ApiTransact) -> Self {
        Self::Transact(t)
    }
}

impl From<ApiQuery> for ApiCall {
    fn from(q: ApiQuery) -> Self {
        Self::Query(q)
    }
}
#[derive(Debug, PartialEq, Eq)]
pub enum ApiQuery {
    GetTransaction,
}

#[derive(Debug, PartialEq, Eq)]
pub enum ApiTransact {
    CreateRequest,
    FundCreateRequest,
    RedeemRequest,
    XandTransfer,
}

impl MockMemberApi {
    pub fn new(bank_balance: i32, xand_balance: i32) -> Self {
        let bank_balance = Usd::try_from_i32(bank_balance).expect("parse i32 into Usd error");
        let xand_balance = Usd::try_from_i32(xand_balance).expect("parse i32 into Usd error");
        let get_txn = GetTransaction::Found(Transaction::default());
        Self {
            bank_balance_return: bank_balance,
            xand_balance_return: xand_balance,
            get_txn_returns: Arc::new(Mutex::new(RefCell::new(Box::new(vec![get_txn].into_iter().cycle())))),
            api_calls: Arc::new(Mutex::new(RefCell::new(vec![]))),
        }
    }

    pub fn test() -> Self {
        Self::new(0, 0)
    }

    pub fn with_get_txn_vec_cycle<I: 'static + IntoIterator<Item = GetTransaction>>(responses: I) -> Self
    where
        <I as IntoIterator>::IntoIter: Clone + Send,
    {
        let iter = responses.into_iter().cycle();
        let mut s = Self::test();
        s.get_txn_returns = Arc::new(Mutex::new(RefCell::new(Box::new(iter))));
        s
    }
}

#[async_trait::async_trait]
impl MemberApi for MockMemberApi {
    async fn get_bank_account_balance(&self, _account_id: i32) -> Result<Usd, Error> {
        Ok(self.bank_balance_return)
    }

    async fn get_xand_balance(&self) -> Result<Usd, Error> {
        Ok(self.xand_balance_return)
    }

    async fn issue_payment(&self, _recipient: Address, _amount: Usd) -> Result<TransactionReceipt, Error> {
        self.api_calls.lock().unwrap().borrow_mut().push(ApiTransact::XandTransfer.into());
        Ok(TransactionReceipt::test())
    }

    async fn issue_create_request(&self, _bank_account: &Account, _amount: Usd) -> Result<TransactionReceipt, Error> {
        self.api_calls.lock().unwrap().borrow_mut().push(ApiTransact::CreateRequest.into());
        Ok(TransactionReceipt::test_with_nonce("id", "nonce"))
    }

    async fn issue_book_transfer(
        &self,
        _bank_account: &Account,
        _create_request: TransactionReceipt,
        amount: Usd,
    ) -> Result<BookTransferReceipt, Error> {
        self.api_calls.lock().unwrap().borrow_mut().push(ApiTransact::FundCreateRequest.into());
        Ok(BookTransferReceipt::test_with(amount.to_minor_units_i32()))
    }

    async fn issue_redeem_request(&self, _bank_account: &Account, _amount: Usd) -> Result<TransactionReceipt, Error> {
        self.api_calls.lock().unwrap().borrow_mut().push(ApiTransact::RedeemRequest.into());
        Ok(TransactionReceipt::test_with_nonce("id", "nonce"))
    }

    async fn get_transaction(&self, _txn_id: &str) -> Result<GetTransaction, Error> {
        self.api_calls.lock().unwrap().borrow_mut().push(ApiQuery::GetTransaction.into());
        Ok(self.get_txn_returns.lock().unwrap().borrow_mut().next().unwrap())
    }
}

#[tokio::test]
async fn pay__enough_xand_balance_issues_1_xand_payment() {
    // Given
    let xand_bal = 200;
    let mock_member_api = MockMemberApi::new(0, xand_bal);
    let doer = ApiDoer::new(mock_member_api.clone());
    let pay_amount = Usd::test_from_u64(200);
    let recipient = Address::test();
    let (send, _rcv) = channel();
    // When
    doer.pay(&Account::dummy_account(), &recipient, pay_amount, send).await.unwrap();

    // Then
    let mut payment_calls = mock_member_api.api_calls.lock().unwrap().take();
    payment_calls.retain(|c| matches!(c, ApiCall::Transact(_)));
    assert_eq!(payment_calls, vec![ApiCall::Transact(ApiTransact::XandTransfer)]);
}

#[tokio::test]
async fn pay__partial_xand_balance_creates_then_issues_payment() {
    // Given
    let xand_bal = 150;
    let bank_bal = 50;
    let mock_member_api = MockMemberApi::new(bank_bal, xand_bal);
    let doer = ApiDoer::new(mock_member_api.clone());
    let pay_amount = Usd::test_from_u64(200);
    let recipient = Address::test();
    let (send, _rcv) = channel();

    // When
    doer.pay(&Account::dummy_account(), &recipient, pay_amount, send).await.unwrap();

    // Then
    let mut api_calls = mock_member_api.api_calls.lock().unwrap().take();
    api_calls.retain(|c| matches!(c, ApiCall::Transact(..)));
    assert_eq!(
        api_calls,
        vec![
            ApiTransact::CreateRequest.into(),
            ApiTransact::FundCreateRequest.into(),
            ApiTransact::XandTransfer.into()
        ]
    );
}

#[tokio::test]
async fn pay__insufficent_balances_returns_error() {
    // Given
    let xand_bal = 100;
    let bank_bal = 100;
    let mock_member_api = MockMemberApi::new(bank_bal, xand_bal);
    let doer = ApiDoer::new(mock_member_api.clone());
    let pay_amount = Usd::test_from_u64(250);
    let recipient = Address::test();
    let (send, _rcv) = channel();

    // When
    let err = doer.pay(&Account::dummy_account(), &recipient, pay_amount, send).await.unwrap_err();

    // Then
    assert!(matches!(err, error::Error::InsufficientFunds));
}

#[tokio::test]
async fn create__insufficent_balances_returns_error() {
    // Given
    let xand_bal = 0;
    let bank_bal = 0;
    let mock_member_api = MockMemberApi::new(bank_bal, xand_bal);
    let doer = ApiDoer::new(mock_member_api.clone());
    let amt = Usd::test_from_u64(250);
    let (sender, _receiver) = channel();

    // When
    let err = doer.create(&Account::dummy_account(), amt, sender).await.unwrap_err();

    // Then
    assert!(matches!(err, error::Error::InsufficientFunds));
}

#[tokio::test]
async fn redeem__insufficent_balances_returns_error() {
    // Given
    let xand_bal = 100;
    let bank_bal = 0;
    let mock_member_api = MockMemberApi::new(bank_bal, xand_bal);
    let doer = ApiDoer::new(mock_member_api.clone());
    let amt = Usd::test_from_u64(250);
    let (sender, _receiver) = channel();

    // When
    let res = doer.redeem(&Account::dummy_account(), amt, sender).await;

    // Then
    assert!(matches!(res.unwrap_err(), error::Error::InsufficientFunds));
}

#[tokio::test]
async fn redeem__sufficient_balances_submits_redeem_request_to_api() {
    // Given
    let xand_bal = 100;
    let bank_bal = 0;
    let mock_member_api = MockMemberApi::new(bank_bal, xand_bal);
    let doer = ApiDoer::new(mock_member_api.clone());
    let amt = Usd::test_from_u64(100);
    let (sender, _receiver) = channel();

    // When
    doer.redeem(&Account::dummy_account(), amt, sender).await.unwrap();

    // Then
    let mut api_calls = mock_member_api.api_calls.lock().unwrap().take();
    api_calls.retain(|c| matches!(c, ApiCall::Transact(_)));
    assert_eq!(api_calls, vec![ApiTransact::RedeemRequest.into(),]);
}
