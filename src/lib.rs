#![cfg_attr(not(debug_assertions), deny(warnings))] // Forbid warnings in release builds
#![warn(clippy::all, clippy::nursery, clippy::pedantic, rust_2018_idioms)]
// Safety-critical application lints
#![deny(
    clippy::pedantic,
    clippy::float_cmp_const,
    clippy::indexing_slicing,
    clippy::integer_arithmetic,
    clippy::unwrap_used
)]
// For `slint`
#![allow(
    clippy::cast_lossless,
    clippy::cast_possible_truncation,
    clippy::cast_sign_loss,
    elided_lifetimes_in_paths,
    clippy::if_not_else,
    clippy::integer_arithmetic,
    clippy::indexing_slicing,
    clippy::items_after_statements,
    clippy::must_use_candidate,
    clippy::needless_pass_by_value,
    clippy::no_effect_underscore_binding,
    clippy::semicolon_if_nothing_returned,
    clippy::similar_names,
    clippy::too_many_lines,
    clippy::unreadable_literal,
    clippy::use_self,
    clippy::useless_transmute,
    clippy::unwrap_used,
    clippy::used_underscore_binding
)]
#![allow(
    clippy::enum_glob_use,
    clippy::implicit_return,
    clippy::iter_nth_zero,
    clippy::match_bool,
    clippy::missing_errors_doc,
    clippy::module_name_repetitions,
    clippy::wildcard_imports
)]
// To use the `unsafe` keyword, do not remove the `unsafe_code` attribute entirely.
// Instead, change it to `#![allow(unsafe_code)]` or preferably `#![deny(unsafe_code)]` + opt-in
// with local `#[allow(unsafe_code)]`'s on a case-by-case basis, if practical.
#![forbid(unsafe_code)]
#![forbid(bare_trait_objects)]
#![cfg_attr(test, allow(non_snake_case))]
// Uncomment before ship to reconcile use of possibly redundant crates, debug remnants, missing
// license files and more
// #![allow(clippy::blanket_clippy_restriction_lints)]
// #![warn(clippy::cargo, clippy::restriction, missing_docs)]
// #![allow(clippy::implicit_return)]

#[macro_use]
extern crate serde;

mod adapters;
// TODO: Remove this once backend is "connected" to frontend
#[allow(dead_code)]
mod backend;
mod error;
mod handlers;
mod traits;

use crate::backend::domain_state::ui_handler::{FollowupAction, UiHandlerProvider};
use crate::backend::{
    domain_state::{
        data::{Account, Address, Usd},
        DomainState,
    },
    domain_state_holder::DomainStateHolder,
    xandbox_data::EntitiesMetadataLoader,
};
pub use error::{Error, Result};
pub use traits::{DigitGroupingPolicy, DigitGroupingsExt};

use crate::backend::domain_state::IState;
use crate::backend::ui_state::UiStateHandle;
use slint_generated::*;

#[allow(clippy::all, clippy::pedantic)]
mod slint_generated {
    slint::include_modules!();
}

#[cfg_attr(target_arch = "wasm32", wasm_bindgen::prelude::wasm_bindgen(start))]
#[cfg(target_arch = "wasm32")]
pub fn wasm_entry() -> Result<(), wasm_bindgen::JsValue> {
    app().unwrap();
    Ok(())
}
/// # Panics
///
/// Will panic if using incorrect adatpers given environment (wasm vs native), and,
/// for many `unwrap()`s used so far.
pub fn app() -> Result<()> {
    let app_window = AppWindow::new();

    // Get/Set GlobalState from ui
    let mut state = DomainState::default();
    let permissioned_members = EntitiesMetadataLoader::load_permissioned_static();

    state.possible_members = permissioned_members.into_iter().map(Into::into).collect();
    state.possible_bank_accounts = Some(vec![Account::dummy_account()]);
    state.ui_state = UiStateHandle::new(app_window.as_weak());

    // Set initial global_state properties
    state.initialize_global_ui_state();

    // Build UiHandler provider
    let (sender, receiver) = std::sync::mpsc::channel();
    let ui_handler_provider = UiHandlerProvider::new(sender.clone());
    let ui_handler = ui_handler_provider.new_ui_handler_for(state.get_current_member());

    // Wrap the state in its sharable container
    let state_container = DomainStateHolder::new(state);

    ui_handler.update_balances(&Account::dummy_account(), state_container.clone());

    // Assign handlers for natively-implemented callbacks
    handlers::assign(&app_window, &state_container, &ui_handler_provider);

    use slint::{Timer, TimerMode};
    let timer = Timer::default();

    // Process messages every 100ms
    let update_interval_ms = 100;

    let force_refresh_every_n_updates = 50;
    let mut _counter: usize = 0;
    timer.start(TimerMode::Repeated, std::time::Duration::from_millis(update_interval_ms), move || {
        // If any data available in set of promises, `update` to pull it into the state
        let mut state = state_container.state.as_ref().borrow_mut();
        state.update();
        // Explicitly drop mutable reference so state_container can be cloned (and borrowed) later in this function
        drop(state);

        // Process any FollowupAction notifications

        while let Ok(msg) = receiver.try_recv() {
            dbg!(&msg);
            match msg {
                FollowupAction::RefreshBalances => {
                    let state = state_container.state.as_ref().borrow();
                    let current_member = state.get_current_member();
                    let current_account = state.get_current_bank_account().unwrap();
                    drop(state);
                    let ui_handler = ui_handler_provider.new_ui_handler_for(current_member);
                    ui_handler.update_balances(&current_account, state_container.clone());
                },
            }
        }

        if _counter % force_refresh_every_n_updates == 0 {
            if let Err(e) = sender.send(FollowupAction::RefreshBalances) {
                dbg!(e);
            }
        }
        _counter += 1;
    });

    app_window.run();

    Ok(())
}
