pub mod member_api_client;
/// This module was auto-generated from Member API's swagger doc and scooped over here.
/// See `rust-client` branch in member api
/// See this commit for mods made to generated content: `https://gitlab.com/TransparentIncDevelopment/product/apps/member_api/-/commit/6639ea16686af873a64578dbd39874bd163cf9ab`
/// And this makefile to re-generate: `https://gitlab.com/TransparentIncDevelopment/product/apps/member_api/blob/6639ea16686af873a64578dbd39874bd163cf9ab/client-rs/Makefile#L7-L7`
#[allow(dead_code, clippy::use_self, clippy::missing_const_for_fn)]
pub mod models;
