#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct JwtInfo {
    pub secret: JwtSecret,
    pub token: Jwt,
}

#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct JwtSecret(pub String);

#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct Jwt(pub String);
