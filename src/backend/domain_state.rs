use crate::backend::domain_state::create_promise::CreatePromise;
use crate::backend::domain_state::data::{Account, MemberData, Usd};
use crate::backend::domain_state::payment_promise::{PaymentPromise, PaymentStatus};
use crate::backend::domain_state::redeem_promise::RedeemPromise;
use crate::backend::fake_company_names::{index_to_capital_letter, FAKE_COMPANIES};
use crate::backend::ui_state::UiStateHandle;
use crate::EntitiesMetadataLoader;
use poll_promise::Promise;

#[allow(clippy::future_not_send)]
pub mod api_doer;
pub mod data;
pub mod ui_handler;

#[derive(Default)]
pub struct DomainState {
    pub possible_members: Vec<MemberData>,
    pub possible_bank_accounts: Option<Vec<Account>>,
    pub errors: Vec<String>,
    pub promises: StatePromises,
    pub ui_state: UiStateHandle,
}

/// Methods that handlers will rely on to get things like `current_member()`, `current_bank()`
pub trait IState {
    fn get_current_member(&self) -> MemberData;
    fn get_current_bank_account(&self) -> Option<Account>;
    fn get_current_member_name(&self) -> String;
}

impl IState for DomainState {
    fn get_current_member(&self) -> MemberData {
        let index = self.ui_state.get_current_member_index();
        let index = index as usize;
        let member = self.possible_members.get(index).unwrap();
        member.clone()
    }
    fn get_current_bank_account(&self) -> Option<Account> {
        let index = self.ui_state.get_current_bank_account_index();
        let index = index as usize;
        self.possible_bank_accounts.as_ref().and_then(|accts| accts.get(index)).map(Clone::clone)
    }

    fn get_current_member_name(&self) -> String {
        let index = self.ui_state.get_current_member_index();
        let index = index as usize;
        FAKE_COMPANIES.get(index).map_or(index_to_capital_letter(index).to_string(), ToString::to_string)
    }
}

#[derive(PartialEq, Eq, Debug)]
pub struct DomainUiRecipient {
    pub address: String,
    pub friendly_name: String,
}
impl DomainState {
    pub fn possible_recipients(&self) -> Vec<DomainUiRecipient> {
        let current = self.get_current_member().address;

        self.possible_members
            .iter()
            .enumerate()
            .map(|(idx, m)| {
                let friendly_name = FAKE_COMPANIES
                    .get(idx)
                    .map(ToString::to_string)
                    .unwrap_or(format!("Counterparty {}", index_to_capital_letter(idx)));
                DomainUiRecipient { address: m.address.clone(), friendly_name }
            })
            .filter(|m| m.address != current)
            .collect()
    }

    pub fn set_network_spec(&mut self, spec: String) -> Result<(), String> {
        let members = EntitiesMetadataLoader::parse(&spec)?;
        self.possible_members = members.into_iter().map(Into::into).collect();

        // call initialize global state again
        self.initialize_global_ui_state();
        // Set the bank balance texts to "...loading..."

        self.ui_state.clear_bank_balance();
        self.ui_state.clear_xand_balance();

        //
        Ok(())
    }

    pub fn initialize_global_ui_state(&self) {
        self.ui_state.initialize_possible_members(&self.possible_members);
        self.ui_state.initialize_possible_bank_accounts(&self.possible_bank_accounts.as_ref().unwrap().clone());
    }

    pub fn update(&mut self) {
        let current_bank_bal_data = Self::check_promise(&self.promises.bank_bal_promise);
        match current_bank_bal_data {
            None => {},
            Some(bal_res) => {
                self.ui_state.update_bank_bal(bal_res);
                self.promises.bank_bal_promise = None;
            },
        }

        let current_xand_bal = Self::check_promise(&self.promises.xand_bal_promise);
        match current_xand_bal {
            None => {},
            Some(bal_res) => {
                self.ui_state.update_xand_bal(bal_res);
                self.promises.xand_bal_promise = None;
            },
        }

        if let Some(ref mut promise) = &mut self.promises.create_promise {
            let updates = promise.advance();
            self.ui_state.update_create_status(updates);
            if let Some(_receipt) = promise.ready() {
                self.promises.create_promise = None;
            }
        }

        if let Some(ref mut promise) = &mut self.promises.payment_promise {
            let status = promise.advance();
            match status {
                PaymentStatus::InProgress(progress) => {
                    self.ui_state.update_payment_status(progress);
                },
                PaymentStatus::Complete(progress) => {
                    self.ui_state.update_payment_status(progress);
                    self.promises.payment_promise = None;
                },
            }
        }

        if let Some(ref mut promise) = &mut self.promises.redeem_promise {
            let updates = promise.advance();
            self.ui_state.update_redeem_status(updates);
            if let Some(_receipt) = promise.ready() {
                self.promises.redeem_promise = None;
            }
        }
    }

    fn check_promise<T: Send, E: Send>(promise: &EventualData<T, E>) -> Option<&Result<T, E>> {
        let x = promise.as_ref().and_then(Promise::ready);
        x
    }
}
pub type EventualData<T, E = api_doer::error::Error> = Option<Promise<Result<T, E>>>;

#[derive(Default)]
pub struct StatePromises {
    pub bank_bal_promise: EventualData<Usd>,
    pub xand_bal_promise: EventualData<Usd>,
    pub create_promise: Option<CreatePromise>,
    pub redeem_promise: Option<RedeemPromise>,
    pub payment_promise: Option<PaymentPromise>,
}

pub mod create_promise {
    use crate::backend::domain_state::api_doer;
    use crate::backend::domain_state::data::CreateReceipt;
    use crate::backend::domain_state::payment_promise::PaymentUpdate;
    use poll_promise::Promise;
    use std::sync::mpsc::Receiver;

    pub type CreateResult = Result<CreateReceipt, api_doer::error::Error>;
    /// Struct giving access to updates while Payment flow is ongoing
    pub struct CreatePromise {
        promise: Promise<CreateResult>,
        updates_receiver: Receiver<PaymentUpdate>,
        updates: Vec<PaymentUpdate>,
    }

    impl CreatePromise {
        pub fn new(promise: Promise<CreateResult>, updates_receiver: Receiver<PaymentUpdate>) -> Self {
            Self { promise, updates_receiver, updates: vec![] }
        }

        pub fn advance(&mut self) -> Vec<PaymentUpdate> {
            while let Ok(msg) = self.updates_receiver.try_recv() {
                self.updates.push(msg);
            }
            self.updates.clone()
        }

        pub fn ready(&self) -> Option<&CreateResult> {
            self.promise.ready()
        }
    }
}

pub mod redeem_promise {
    use crate::backend::domain_state::api_doer;
    use crate::backend::domain_state::data::RedeemReceipt;
    use crate::backend::runtime_adapter::time::Timestamp;
    use poll_promise::Promise;
    use std::sync::mpsc::Receiver;

    pub type RedeemResult = Result<RedeemReceipt, api_doer::error::Error>;
    /// Struct giving access to updates while Payment flow is ongoing
    pub struct RedeemPromise {
        promise: Promise<RedeemResult>,
        updates_receiver: Receiver<RedeemUpdate>,
        updates: Vec<RedeemUpdate>,
    }

    impl RedeemPromise {
        pub fn new(promise: Promise<RedeemResult>, updates_receiver: Receiver<RedeemUpdate>) -> Self {
            Self { promise, updates_receiver, updates: vec![] }
        }

        pub fn advance(&mut self) -> Vec<RedeemUpdate> {
            while let Ok(msg) = self.updates_receiver.try_recv() {
                self.updates.push(msg);
            }
            self.updates.clone()
        }

        pub fn ready(&self) -> Option<&RedeemResult> {
            self.promise.ready()
        }
    }

    #[derive(Clone)]
    pub enum RedeemUpdate {
        Submitted(Timestamp),
        Finalized(Timestamp),
        Fulfilled(Timestamp),
    }

    impl RedeemUpdate {
        pub fn submitted() -> Self {
            Self::Submitted(Timestamp::now())
        }
        pub fn finalized() -> Self {
            Self::Finalized(Timestamp::now())
        }
        pub fn fulfilled() -> Self {
            Self::Fulfilled(Timestamp::now())
        }
    }
}

pub mod payment_promise {
    use crate::backend::clients::models::Transaction;
    use crate::backend::domain_state::api_doer;
    use crate::backend::domain_state::data::{PaymentReceipt, TransactionReceipt};
    use crate::backend::runtime_adapter::time::Timestamp;
    use poll_promise::Promise;
    use std::sync::mpsc::Receiver;

    pub type PaymentResult = Result<PaymentReceipt, api_doer::error::Error>;
    /// Struct giving access to updates while Payment flow is ongoing
    pub struct PaymentPromise {
        promise: Promise<PaymentResult>,
        updates_receiver: Receiver<PaymentUpdate>,
        updates: Vec<PaymentUpdate>,
    }

    impl PaymentPromise {
        pub fn new(
            promise: Promise<Result<PaymentReceipt, api_doer::error::Error>>,
            updates_receiver: Receiver<PaymentUpdate>,
        ) -> Self {
            PaymentPromise { promise, updates_receiver, updates: vec![] }
        }

        pub fn advance(&mut self) -> PaymentStatus {
            while let Ok(msg) = self.updates_receiver.try_recv() {
                self.updates.push(msg);
            }

            if let Some(_result) = self.ready() {
                PaymentStatus::Complete(self.updates.clone())
            } else {
                PaymentStatus::InProgress(self.updates.clone())
            }
        }

        pub fn ready(&self) -> Option<&PaymentResult> {
            self.promise.ready()
        }
    }

    pub enum PaymentStatus {
        InProgress(Vec<PaymentUpdate>),
        Complete(Vec<PaymentUpdate>),
    }

    #[derive(Clone)]
    pub enum CreateUpdate {
        CreateRequestSubmitted(Timestamp, TransactionReceipt),
        CreateRequestAccepted(Timestamp),
        BookTransferSubmitted(Timestamp),
        BookTransferAccepted(Timestamp),
        CashConfirmation(Timestamp, Transaction),
    }

    impl CreateUpdate {
        pub fn create_request_submitted(tx_receipt: TransactionReceipt) -> Self {
            let ts = Timestamp::now();
            Self::CreateRequestSubmitted(ts, tx_receipt)
        }

        pub fn create_request_accepted() -> Self {
            let ts = Timestamp::now();
            Self::CreateRequestAccepted(ts)
        }

        pub fn book_transfer_submitted() -> Self {
            let ts = Timestamp::now();
            Self::BookTransferSubmitted(ts)
        }

        pub fn book_transfer_accepted() -> Self {
            let ts = Timestamp::now();
            Self::BookTransferAccepted(ts)
        }

        pub fn cash_confirmation(tx: Transaction) -> Self {
            let ts = Timestamp::now();
            Self::CashConfirmation(ts, tx)
        }
    }

    #[derive(Clone)]
    pub enum PaymentUpdate {
        CreateUpdate(CreateUpdate),
        PaymentSubmitted(Timestamp, TransactionReceipt),
        PaymentFinalized(Timestamp, Transaction),
    }

    impl PaymentUpdate {
        pub fn payment_submitted(tx: TransactionReceipt) -> Self {
            Self::PaymentSubmitted(Timestamp::now(), tx)
        }

        pub fn payment_finalized(tx: Transaction) -> Self {
            Self::PaymentFinalized(Timestamp::now(), tx)
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::{AppWindow, ComponentHandle};

    #[test]
    fn possible_recipients__filters_out_current_member() {
        // Given
        let app_window = AppWindow::new();
        let ui_state = UiStateHandle::new(app_window.as_weak());

        let current = MemberData::test_with("5DesW2g9miJMahngjaEVHap6kBMiDH33CdgndL5d75GkY7oz");
        let other = MemberData::test_with("5GedXksPiCaXqv114r2NPQQbgirgA9X8pyRM1AHZ42bDGomj");
        let s = DomainState { possible_members: vec![current, other.clone()], ui_state, ..DomainState::default() };

        // When
        let possible_recipients = s.possible_recipients();

        // Then
        let possible_addresses: Vec<String> = possible_recipients.into_iter().map(|r| r.address).collect();
        assert_eq!(possible_addresses, vec![other.address]);
    }
}
