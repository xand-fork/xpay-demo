pub const FAKE_COMPANIES: &[&str] = &[
    "General Motors Corp",
    "Mitsubishi Electric Co",
    // "AliceCo",
    // "BobCorp",
    // "Dunder Mifflin",
    // "CryptOTC",
    // "Bubba Gump Shrimp Company",
    // "Wonka Industries",
    // "Initech",
    // "Stark Industries",
    // "Prestige Worldwide",
    // "Waystar RoyCo",
    // "Crackin' Crypto",
];

pub fn index_to_capital_letter(i: usize) -> char {
    let a_index = b'A';
    let i_u8: u8 = i.try_into().unwrap_or(0);
    let c = a_index + i_u8;
    c as char
}
#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn index_to_capital_letter__numbers_maps_to_capital_letters() {
        // Given
        let a_index = 0;
        let z_index = 25;

        // When
        let a = index_to_capital_letter(a_index);
        let z = index_to_capital_letter(z_index);

        // Then
        assert_eq!(a, 'A');
        assert_eq!(z, 'Z')
    }

    #[test]
    fn index_to_capital_letter__overflow_usize_returns_A() {
        // Given
        let i: usize = usize::MAX;

        // When
        let a = index_to_capital_letter(i);

        // Then
        assert_eq!(a, 'A');
    }
}
