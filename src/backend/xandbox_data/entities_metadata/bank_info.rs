#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
pub struct BankInfo {
    pub bank: Bank,
    pub account: Account,
}

#[derive(Debug, Clone, PartialEq, Copy, Eq, Hash, Serialize, Deserialize /*, strum::ToString, strum::EnumIter*/)]
#[serde(rename_all = "kebab-case")]
pub enum Bank {
    Mcb,
    Provident,
    McbFailing,

    // dummy banks for Xandbox
    TestBankOne,
    TestBankTwo,
}

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
pub struct Account {
    routing_number: u32,
    account_number: u32,
}
