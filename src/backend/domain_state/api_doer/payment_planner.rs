use crate::backend::domain_state::data::Usd;

pub struct Balances {
    bank: Usd,
    xand: Usd,
}

impl Balances {
    pub const fn new(bank: Usd, xand: Usd) -> Self {
        Self { bank, xand }
    }
}

#[cfg(test)]
impl Balances {
    pub fn test_new(bank: u64, xand: u64) -> Self {
        Self { bank: Usd::test_from_u64(bank), xand: Usd::test_from_u64(xand) }
    }
}

#[derive(Copy, Clone, Debug, PartialEq, Eq)]
pub struct Payment {
    amount: Usd,
}

impl Payment {
    pub const fn new(amount: Usd) -> Self {
        Self { amount }
    }

    pub const fn usd(&self) -> Usd {
        self.amount
    }
}

pub struct PaymentPlanner {
    balances: Balances,
}

impl PaymentPlanner {
    pub const fn new(balances: Balances) -> Self {
        Self { balances }
    }

    /// Given balances and an intent to pay a counterparty, returns steps to achieve transfer of funds
    pub fn get_steps_for(&self, payment: Payment) -> StepsResult {
        if self.balances.xand >= payment.amount {
            let xand_transfer = Step::XandTransfer(payment);
            return StepsResult::Possible(vec![xand_transfer]);
        }

        let need_extra: Usd = payment.amount.sub(self.balances.xand);
        if self.balances.bank >= need_extra {
            let create = Step::Create(need_extra);
            let xand_transfer = Step::XandTransfer(payment);
            return StepsResult::Possible(vec![create, xand_transfer]);
        }

        StepsResult::InsufficientFunds
    }
}

#[derive(Debug)]
pub enum StepsResult {
    Possible(Vec<Step>),
    InsufficientFunds,
}

#[cfg(test)]
impl StepsResult {
    pub fn steps(&self) -> Option<Vec<Step>> {
        match self {
            StepsResult::Possible(steps) => Some(steps.clone()),
            StepsResult::InsufficientFunds => None,
        }
    }
}

#[derive(Debug, Clone, Eq, PartialEq)]
pub enum Step {
    Create(Usd),
    XandTransfer(Payment),
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::backend::domain_state::data::Usd;

    #[test]
    fn get_steps_for__sufficient_xand_balance_for_payment_returns_1_step() {
        // Given
        let xand_balance = 1_000;
        let balances = Balances::test_new(0, xand_balance);
        let planner = PaymentPlanner::new(balances);

        let amount = Usd::test_from_u64(500);
        let intent = Payment::new(amount);

        // When
        let res = planner.get_steps_for(intent);

        // Then
        assert!(matches!(res, StepsResult::Possible(_)));
        let steps = res.steps().expect("Expected Some; Found None");
        assert_eq!(steps, vec![Step::XandTransfer(intent)]);
    }

    #[test]
    fn get_steps_for__amount_greater_than_balances_returns_insufficient_funds() {
        // Given
        let balances = Balances::test_new(0, 0);
        let planner = PaymentPlanner::new(balances);

        let amount = Usd::test_from_u64(500);
        let intent = Payment::new(amount);

        // When
        let res = planner.get_steps_for(intent);

        // Then
        assert!(matches!(res, StepsResult::InsufficientFunds));
    }

    #[test]
    fn get_steps_for__sufficient_bank_and_zero_xand_returns_2_steps() {
        // Given
        let balances = Balances::test_new(100, 0);
        let planner = PaymentPlanner::new(balances);

        let amount = Usd::test_from_u64(100);
        let intent = Payment::new(amount);

        // When
        let res = planner.get_steps_for(intent);

        // Then
        let steps = res.steps().expect("Expected Some; Found None");
        assert_eq!(steps, vec![Step::Create(amount), Step::XandTransfer(intent)]);
    }

    #[test]
    fn get_steps_for__sufficient_bank_and_partial_returns_two_steps() {
        // Given
        let balances = Balances::test_new(100, 25);
        let planner = PaymentPlanner::new(balances);

        let amount = Usd::test_from_u64(100);
        let intent = Payment::new(amount);

        // When
        let res = planner.get_steps_for(intent);

        // Then
        let steps = res.steps().expect("Expected Some; Found None");
        assert_eq!(steps, vec![Step::Create(Usd::test_from_u64(75)), Step::XandTransfer(intent)]);
    }
}
