use crate::backend::{
    clients::{member_api_client::error::Error, models},
    domain_state::data::{Account, Address, BookTransferReceipt, TransactionReceipt, Usd},
};
use ehttp::Request;
use futures::channel::oneshot;
use std::collections::BTreeMap;
use url::Url;

pub mod error {
    use crate::backend::{clients::member_api_client::ResponseFailureReport, domain_state::data::usd};
    use futures::channel::oneshot::Canceled;
    use std::str::Utf8Error;
    use thiserror::Error;

    #[derive(Debug, Error)]
    pub enum Error {
        #[error("CreateRequestMissingNonce")]
        CreateRequestMissingNonce,
        #[error(transparent)]
        DeserializeJson(#[from] serde_json::Error),
        #[error("{}", .0)]
        Ehttp(ehttp::Error),
        #[error("{}", .0)]
        EhttpChannel(#[from] Canceled),
        #[error("Request Failed: {:?}", .report)]
        RequestFailure { report: ResponseFailureReport },
        #[error("{}. Could not parse to str: {:?}", .0, .1)]
        ResponseBodyToStr(Utf8Error, Vec<u8>),
        #[error(transparent)]
        UsdConversion(#[from] usd::Error),
    }
}

#[async_trait::async_trait]
pub trait MemberApi {
    async fn get_bank_account_balance(&self, account_id: i32) -> Result<Usd, error::Error>;
    async fn get_xand_balance(&self) -> Result<Usd, error::Error>;
    async fn issue_payment(&self, recipient: Address, amount: Usd) -> Result<TransactionReceipt, error::Error>;
    async fn issue_create_request(
        &self,
        bank_account: &Account,
        amount: Usd,
    ) -> Result<TransactionReceipt, error::Error>;
    async fn issue_book_transfer(
        &self,
        bank_account: &Account,
        create_request: TransactionReceipt,
        amount: Usd,
    ) -> Result<BookTransferReceipt, error::Error>;
    async fn issue_redeem_request(
        &self,
        bank_account: &Account,
        amount: Usd,
    ) -> Result<TransactionReceipt, error::Error>;

    async fn get_transaction(&self, txn_id: &str) -> Result<GetTransaction, error::Error>;
}

#[derive(Clone)]
pub enum GetTransaction {
    Found(models::Transaction),
    NotFound,
}

#[derive(Debug)]
struct Response(ehttp::Response);

impl Response {
    pub fn try_from(inner_resp: ehttp::Response) -> Result<Self, error::Error> {
        if !inner_resp.ok {
            let report: ResponseFailureReport = inner_resp.into();
            return Err(error::Error::RequestFailure { report });
        }
        Ok(Self(inner_resp))
    }

    pub fn data_as_str(&self) -> Result<&str, error::Error> {
        let bytes = &self.0.bytes;
        let result = std::str::from_utf8(bytes).map_err(|err| error::Error::ResponseBodyToStr(err, bytes.clone()))?;
        Ok(result)
    }
}

#[derive(Debug)]
pub struct ResponseFailureReport {
    pub url: String,
    /// Did we get a 2xx response code?
    pub ok: bool,
    /// Status code (e.g. `404` for "File not found").
    pub status: u16,
    /// Status text (e.g. "File not found" for status code `404`).
    pub status_text: String,
    /// Response body, if any
    pub body: Option<String>,
}

impl From<ehttp::Response> for ResponseFailureReport {
    fn from(resp: ehttp::Response) -> Self {
        let body = resp.text().map(ToString::to_string);
        Self { url: resp.url, ok: resp.ok, status: resp.status, status_text: resp.status_text, body }
    }
}

pub struct MemberApiClient {
    url: Url,
    token: String,
}

impl MemberApiClient {
    pub const fn new(url: Url, token: String) -> Self {
        Self { url, token }
    }

    fn headers(&self) -> BTreeMap<String, String> {
        let jwt = format!("Bearer {}", &self.token);

        ehttp::headers(&[("Authorization", &jwt), ("accept", "application/json"), ("Content-Type", "application/json")])
    }

    async fn execute_request(&self, mut request: Request) -> Result<Response, error::Error> {
        let (sender, receiver) = oneshot::channel();
        request.headers = self.headers();
        // let (sender, promise) = Promise::new();
        ehttp::fetch(request, move |response| {
            let resp: Result<Response, error::Error> =
                response.map_err(error::Error::Ehttp).and_then(Response::try_from);
            dbg!(&resp);
            sender.send(resp).expect("Error sending request response down channel");
        });

        receiver.await?
    }
}

#[async_trait::async_trait]
impl MemberApi for MemberApiClient {
    async fn get_bank_account_balance(&self, account_id: i32) -> Result<Usd, error::Error> {
        let route = format!("api/v1/accounts/{}/balance", account_id);
        let url = self.url.join(&route).expect("Url join should produce valid url");

        let request = ehttp::Request::get(url);
        let response = self.execute_request(request).await?;
        let data_str = response.data_as_str()?;
        let bank_balance: models::BankAccountBalance = serde_json::from_str(data_str)?;

        dbg!(&bank_balance);
        let usd = Usd::try_from_i32(bank_balance.available_balance_in_minor_unit)?;

        Ok(usd)
    }

    async fn get_xand_balance(&self) -> Result<Usd, error::Error> {
        let route = "api/v1/member/balance";
        let url = self.url.join(route).expect("Url join should produce valid url");
        let request = ehttp::Request::get(url);
        let response = self.execute_request(request).await?;
        let data_str = response.data_as_str()?;
        let xand_balance: models::XandBalance = serde_json::from_str(data_str)?;
        dbg!(&xand_balance);
        let usd = Usd::try_from_i32(xand_balance.balance_in_minor_unit)?;
        Ok(usd)
    }

    async fn issue_payment(&self, recipient: Address, amount: Usd) -> Result<TransactionReceipt, error::Error> {
        let route = "api/v1/transactions/claims/send";
        let url = self.url.join(route).expect("Url join should produce valid url");

        let body = models::Payment {
            to_address: Some(recipient.base_58_str()),
            amount_in_minor_unit: Some(amount.to_minor_units_i32()),
        };
        let body_json = serde_json::to_string(&body)?;
        let request = ehttp::Request::post(url, body_json.as_bytes().to_vec());

        let response = self.execute_request(request).await?;
        let data = response.data_as_str()?;
        let receipt: models::Receipt = serde_json::from_str(data)?;
        dbg!(&receipt);
        let txn_receipt = TransactionReceipt::new(receipt);
        Ok(txn_receipt)
    }

    async fn issue_create_request(
        &self,
        bank_account: &Account,
        amount: Usd,
    ) -> Result<TransactionReceipt, error::Error> {
        let route = "api/v1/transactions/claims/create";
        let url = self.url.join(route).expect("Url join should produce valid url");

        let body = models::CreationRequest {
            account_id: bank_account.id(),
            amount_in_minor_unit: amount.to_minor_units_i32(),
        };

        let body_json = serde_json::to_string(&body)?;
        let request = ehttp::Request::post(url, body_json.as_bytes().to_vec());

        let response = self.execute_request(request).await?;
        let data = response.data_as_str()?;
        let receipt: models::Receipt = serde_json::from_str(data)?;
        dbg!(&receipt);
        let txn_receipt = TransactionReceipt::new(receipt);
        Ok(txn_receipt)
    }

    async fn issue_book_transfer(
        &self,
        bank_account: &Account,
        create_request: TransactionReceipt,
        amount: Usd,
    ) -> Result<BookTransferReceipt, Error> {
        let create_txn_id = create_request.txn_id();
        let nonce = create_request.nonce().ok_or(Error::CreateRequestMissingNonce)?;
        let route = format!("api/v1/transactions/claims/{}/fund", create_txn_id);
        let url = self.url.join(&route).expect("Url join should produce valid url");

        let body = models::BankTransferRequest {
            amount_in_minor_unit: amount.to_minor_units_i32(),
            nonce,
            bank_account_id: bank_account.id(),
        };

        let body_json = serde_json::to_string(&body)?;
        let request = ehttp::Request::post(url, body_json.as_bytes().to_vec());

        let response = self.execute_request(request).await?;
        let data = response.data_as_str()?;

        let receipt: models::TransferReceipt = serde_json::from_str(data)?;
        dbg!(&receipt);
        let txn_receipt = BookTransferReceipt::new(create_request, receipt);
        Ok(txn_receipt)
    }

    async fn issue_redeem_request(&self, bank_account: &Account, amount: Usd) -> Result<TransactionReceipt, Error> {
        let route = "api/v1/transactions/claims/redeem";
        let url = self.url.join(route).expect("Url join should produce valid url");

        let body =
            models::Redemption { account_id: bank_account.id(), amount_in_minor_unit: amount.to_minor_units_i32() };

        let body_json = serde_json::to_string(&body)?;
        let request = ehttp::Request::post(url, body_json.as_bytes().to_vec());

        let response = self.execute_request(request).await?;
        let data = response.data_as_str()?;
        let receipt: models::Receipt = serde_json::from_str(data)?;
        dbg!(&receipt);
        let txn_receipt = TransactionReceipt::new(receipt);
        Ok(txn_receipt)
    }

    async fn get_transaction(&self, txn_id: &str) -> Result<GetTransaction, Error> {
        let route = format!("api/v1/transactions/{}", txn_id);
        let url = self.url.join(&route).expect("Url join should produce valid url");

        let request = ehttp::Request::get(url);
        let response_result = self.execute_request(request).await;
        if let Err(err) = response_result {
            return match err {
                Error::RequestFailure { report: ResponseFailureReport { status: 404, .. } } => {
                    Ok(GetTransaction::NotFound)
                },
                _ => Err(err),
            };
        }

        let response = response_result?;
        let data = response.data_as_str()?;
        let txn: models::Transaction = serde_json::from_str(data)?;
        dbg!(&txn);
        Ok(GetTransaction::Found(txn))
    }
}
