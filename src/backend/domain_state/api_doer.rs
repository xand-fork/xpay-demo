use crate::backend::domain_state::payment_promise::{CreateUpdate, PaymentUpdate};
use crate::backend::domain_state::redeem_promise::RedeemUpdate;
use crate::backend::{
    clients::member_api_client::MemberApi,
    domain_state::{
        api_doer::{
            check_txn::CheckTransaction,
            error::Error,
            payment_planner::{Balances, Payment, PaymentPlanner, Step, StepsResult},
        },
        data::{Account, Address, CreateReceipt, PaymentReceipt, RedeemReceipt, Usd},
    },
};
use std::sync::mpsc::Sender;

mod check_txn;
mod payment_planner;
#[cfg(test)]
mod tests;

pub mod error {
    use crate::backend::{
        clients::member_api_client,
        domain_state::{api_doer::check_txn, data::TransactionReceipt},
    };
    use thiserror::Error;

    #[derive(Debug, Error)]
    pub enum Error {
        #[error("{:?}", .0)]
        CreationRequestMissingNonce(TransactionReceipt),
        #[error("{:?}", .0)]
        CheckingConfirmationStatus(#[from] check_txn::Error),
        #[error(transparent)]
        MemberApi(#[from] member_api_client::error::Error),
        #[error("InsufficientFunds")]
        InsufficientFunds,
    }
}

#[derive(Copy, Clone)]
struct ValidCreate<'a>(&'a Account, Usd);

pub struct ApiDoer<M: MemberApi> {
    client: M,
}

impl<M: MemberApi> ApiDoer<M> {
    pub fn new(client: M) -> Self {
        Self { client }
    }

    pub async fn get_account_balance(&self, account: i32) -> Result<Usd, error::Error> {
        let bal_prom = self.client.get_bank_account_balance(account).await;
        bal_prom.map_err(Into::into)
    }

    pub async fn get_xand_balance(&self) -> Result<Usd, error::Error> {
        let bal_prom = self.client.get_xand_balance().await;
        bal_prom.map_err(Into::into)
    }

    async fn check_can_create<'a>(&self, account: &'a Account, amount: Usd) -> Result<ValidCreate<'a>, Error> {
        let bank_balance = self.client.get_bank_account_balance(account.id()).await?;
        if bank_balance < amount {
            return Err(error::Error::InsufficientFunds);
        }
        Ok(ValidCreate(account, amount))
    }

    async fn _create(
        &self,
        create_perm: ValidCreate<'_>,
        updates: Sender<PaymentUpdate>,
    ) -> Result<CreateReceipt, Error> {
        let bank_account = create_perm.0;
        let amount = create_perm.1;
        let create_receipt = self.client.issue_create_request(bank_account, amount).await?;
        updates
            .send(PaymentUpdate::CreateUpdate(CreateUpdate::create_request_submitted(create_receipt.clone())))
            .unwrap();
        if create_receipt.nonce().is_none() {
            return Err(error::Error::CreationRequestMissingNonce(create_receipt));
        }

        let checker = CheckTransaction::timeout_after_60s();
        let _txn = checker.wait_until_finalized(&self.client, &create_receipt).await?;
        updates.send(PaymentUpdate::CreateUpdate(CreateUpdate::create_request_accepted())).unwrap();

        updates.send(PaymentUpdate::CreateUpdate(CreateUpdate::book_transfer_submitted())).unwrap();
        let book_transfer_receipt = self.client.issue_book_transfer(bank_account, create_receipt, amount).await?;
        updates.send(PaymentUpdate::CreateUpdate(CreateUpdate::book_transfer_accepted())).unwrap();

        // Wait until CreationRequest shows "confirmed", meaning trust has fulfilled
        let txn = checker.wait_until_create_is_confirmed(&self.client, &book_transfer_receipt).await?;
        if let Err(e) = updates.send(PaymentUpdate::CreateUpdate(CreateUpdate::cash_confirmation(txn))) {
            dbg!(e);
        }
        Ok(book_transfer_receipt)
    }

    pub async fn create(
        &self,
        bank_account: &Account,
        amount: Usd,
        updates_sender: Sender<PaymentUpdate>,
    ) -> Result<CreateReceipt, error::Error> {
        let valid = self.check_can_create(bank_account, amount).await?;
        self._create(valid, updates_sender).await
    }

    pub async fn pay(
        &self,
        bank_account: &Account,
        recipient: &Address,
        amount: Usd,
        updates_sender: Sender<PaymentUpdate>,
    ) -> Result<PaymentReceipt, error::Error> {
        let bank_balance = self.client.get_bank_account_balance(bank_account.id()).await?;
        let xand_balance = self.client.get_xand_balance().await?;
        let balances = Balances::new(bank_balance, xand_balance);
        let planner = PaymentPlanner::new(balances);
        let plan = planner.get_steps_for(Payment::new(amount));

        let steps = match plan {
            StepsResult::Possible(steps) => steps,
            StepsResult::InsufficientFunds => return Err(error::Error::InsufficientFunds),
        };

        for s in steps {
            match s {
                Step::Create(amt) => {
                    let valid_create = ValidCreate(bank_account, amt);
                    let sender = updates_sender.clone();
                    // All updates sent within fn _create()
                    let _create_receipt = self._create(valid_create, sender).await?;
                },
                Step::XandTransfer(amt) => {
                    let xand_transfer_receipt = self.client.issue_payment(recipient.clone(), amt.usd()).await?;
                    updates_sender.send(PaymentUpdate::payment_submitted(xand_transfer_receipt.clone())).unwrap();
                    let checker = CheckTransaction::timeout_after_60s();
                    let txn = checker.wait_until_confirmed(&self.client, &xand_transfer_receipt).await?;
                    updates_sender.send(PaymentUpdate::payment_finalized(txn)).unwrap();
                },
            };
        }

        Ok(PaymentReceipt::new())
    }

    pub async fn redeem(
        &self,
        bank_account: &Account,
        amount: Usd,
        updates_sender: Sender<RedeemUpdate>,
    ) -> Result<RedeemReceipt, error::Error> {
        let xand_balance = self.client.get_xand_balance().await?;
        if xand_balance < amount {
            return Err(error::Error::InsufficientFunds);
        }
        updates_sender.send(RedeemUpdate::submitted()).unwrap();
        let redeem_receipt = self.client.issue_redeem_request(bank_account, amount).await?;
        let checker = CheckTransaction::timeout_after_60s();
        checker.wait_until_finalized(&self.client, &redeem_receipt).await?;
        updates_sender.send(RedeemUpdate::finalized()).unwrap();

        checker.wait_until_confirmed(&self.client, &redeem_receipt).await?;
        updates_sender.send(RedeemUpdate::fulfilled()).unwrap();
        Ok(RedeemReceipt::new())
    }
}
