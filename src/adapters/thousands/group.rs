use crate::{DigitGroupingPolicy, DigitGroupingsExt};
use rust_decimal::Decimal;
use thousands::Separable;

impl<TIntoDecimal> DigitGroupingsExt for TIntoDecimal
where
    TIntoDecimal: Into<Decimal> + Copy,
{
    fn group_digits(self, policy: DigitGroupingPolicy) -> String {
        self.into().separate_by_policy(policy.into())
    }
}
