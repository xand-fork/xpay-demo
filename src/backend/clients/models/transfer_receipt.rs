// Member API
//
// Member API routes and contracts.
//
// The version of the OpenAPI document: 1.0.0
//
// Generated by: https://openapi-generator.tech

#[derive(Clone, Debug, PartialEq, Default, Serialize, Deserialize)]
pub struct TransferReceipt {
    #[serde(rename = "amountInMinorUnit", skip_serializing_if = "Option::is_none")]
    pub amount_in_minor_unit: Option<i32>,
}

impl TransferReceipt {
    pub fn new() -> TransferReceipt {
        TransferReceipt { amount_in_minor_unit: None }
    }
}
