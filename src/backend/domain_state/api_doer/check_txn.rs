use crate::backend::clients::models::Transaction;
use crate::backend::{
    clients::{
        member_api_client,
        member_api_client::{GetTransaction, MemberApi},
        models::{TransactionState, TransactionStatus},
    },
    domain_state::data::{BookTransferReceipt, TransactionReceipt},
};
use std::time::Duration;
use thiserror::Error;

pub struct CheckTransaction {
    retry_interval: Duration,
    max_retries: usize,
}

#[derive(Debug, Error)]
pub enum Error {
    #[error(transparent)]
    MemberApi(#[from] member_api_client::error::Error),
    #[error("Timed out waiting for confirmation: {}",.0)]
    TimeoutExceededAwaitingConfirmation(String),
    #[error("Timed out waiting for finalization: {}",.0)]
    TimeoutExceededAwaitingFinalization(String),
    #[error("Transaction {txn_id} failed. State {state:?}. Reason {reason:?}")]
    TransactionFailed { txn_id: String, state: TransactionState, reason: Option<String> },
}

impl CheckTransaction {
    pub const fn timeout_after_60s() -> Self {
        Self { retry_interval: Duration::from_millis(500), max_retries: 120 }
    }

    pub async fn wait_until_create_is_confirmed<M: MemberApi>(
        &self,
        client: &M,
        book_transfer: &BookTransferReceipt,
    ) -> Result<Transaction, Error> {
        self.wait_until_confirmed(client, &book_transfer.create_txn_receipt()).await
    }

    async fn wait_until_predicate<M: MemberApi>(
        &self,
        client: &M,
        txn_receipt: &TransactionReceipt,
        predicate: impl Fn(&Transaction) -> Result<bool, Error>,
    ) -> Result<Transaction, Error> {
        let txn_id = txn_receipt.txn_id();
        let mut tries = 0;
        while tries < self.max_retries {
            let get_txn = client.get_transaction(&txn_id).await?;
            if let GetTransaction::Found(txn) = get_txn {
                match predicate(&txn) {
                    Ok(true) => return Ok(txn),
                    Ok(false) => { /*Continue looping*/ },
                    Err(e) => return Err(e),
                }
            }
            tries = tries.saturating_add(1);
            crate::backend::runtime_adapter::sleep(self.retry_interval).await;
        }
        Err(Error::TimeoutExceededAwaitingFinalization(txn_id))
    }
    pub async fn wait_until_finalized<M: MemberApi>(
        &self,
        client: &M,
        txn_receipt: &TransactionReceipt,
    ) -> Result<Transaction, Error> {
        // If found txn, return true
        let is_found = |_txn: &Transaction| -> std::result::Result<bool, Error> { Ok(true) };
        self.wait_until_predicate(client, txn_receipt, is_found).await
    }

    pub async fn wait_until_confirmed<M: MemberApi>(
        &self,
        client: &M,
        txn_receipt: &TransactionReceipt,
    ) -> Result<Transaction, Error> {
        let is_confirmed = |txn: &Transaction| {
            let txn_id = txn.transaction_id.clone();
            let TransactionStatus { state, details } = *(txn.clone().status);
            match state {
                TransactionState::Confirmed => return Ok(true),
                TransactionState::Cancelled => {
                    return Err(Error::TransactionFailed {
                        txn_id,
                        state: TransactionState::Cancelled,
                        reason: details,
                    })
                },
                TransactionState::Pending => { /* Keep waiting */ },
                TransactionState::Invalid => {
                    return Err(Error::TransactionFailed { txn_id, state: TransactionState::Invalid, reason: details })
                },
            }
            Ok(false)
        };
        self.wait_until_predicate(client, txn_receipt, is_confirmed).await
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::backend::{
        clients::models::Transaction,
        domain_state::api_doer::tests::{ApiCall, ApiQuery, MockMemberApi},
    };

    #[tokio::test]
    async fn wait_until_confirmed__returns_ok_if_status_is_confirmed() {
        // Given
        let client = MockMemberApi::with_get_txn_vec_cycle([GetTransaction::Found(Transaction::default())]);
        let txn_receipt = TransactionReceipt::test();

        let mut checker = CheckTransaction::timeout_after_60s();
        checker.retry_interval = Duration::ZERO;

        // When
        let res = checker.wait_until_confirmed(&client, &txn_receipt).await;

        // Then
        assert!(res.is_ok());
        let mut calls = client.api_calls.lock().expect("Could not get lock on mutex in test").take();
        calls.retain(|call| matches!(call, ApiCall::Query(_)));
        assert_eq!(calls.len(), 1);
    }

    #[tokio::test]
    async fn wait_until_confirmed__returns_err_if_status_is_invalid() {
        // Given
        let mut txn = Transaction::default();
        txn.status.state = TransactionState::Invalid;
        let client = MockMemberApi::with_get_txn_vec_cycle([GetTransaction::Found(txn)]);
        let txn_receipt = TransactionReceipt::test();

        let mut checker = CheckTransaction::timeout_after_60s();
        checker.retry_interval = Duration::ZERO;

        // When
        let res = checker.wait_until_confirmed(&client, &txn_receipt).await;

        // Then
        assert!(res.is_err());
    }

    #[tokio::test]
    async fn wait_until_confirmed__retries_check_if_not_found() {
        // Given
        let returns =
            vec![GetTransaction::NotFound, GetTransaction::NotFound, GetTransaction::Found(Transaction::default())];
        let client = MockMemberApi::with_get_txn_vec_cycle(returns.into_iter());
        let txn_receipt = TransactionReceipt::test();

        let mut checker = CheckTransaction::timeout_after_60s();
        checker.retry_interval = Duration::ZERO;

        // When
        let res = checker.wait_until_confirmed(&client, &txn_receipt).await;

        // Then
        assert!(res.is_ok());
        let mut calls = client.api_calls.lock().expect("Could not get lock on mutex in test").take();
        calls.retain(|c| matches!(c, ApiCall::Query(ApiQuery::GetTransaction)));
        assert_eq!(calls.len(), 3);
    }

    #[tokio::test]
    async fn wait_until_confirmed__after_max_retries_returns_timeout_err() {
        // Given
        let returns = vec![GetTransaction::NotFound];
        let client = MockMemberApi::with_get_txn_vec_cycle(returns.into_iter());
        let txn_receipt = TransactionReceipt::test();

        let mut checker = CheckTransaction::timeout_after_60s();
        checker.retry_interval = Duration::ZERO;
        checker.max_retries = 10;

        // When
        let res = checker.wait_until_confirmed(&client, &txn_receipt).await.unwrap_err();

        // Then
        assert!(matches!(res, Error::TimeoutExceededAwaitingFinalization(_)));
        let mut calls = client.api_calls.lock().expect("Could not get lock on mutex in test").take();
        calls.retain(|c| matches!(c, ApiCall::Query(ApiQuery::GetTransaction)));
        assert_eq!(calls.len(), 10);
    }
}
