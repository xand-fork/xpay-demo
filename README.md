# xpay-demo
MVP payment applet designed as a Transparent Systems sales aid.

# Launch Instructions
## Launch Xandbox
* Download the latest [Xandbox](https://transparentinc.jfrog.io/ui/repos/tree/General/artifacts-external%2Fxandbox%2Fxandbox_1.0.1.zip).
* Unzip it and follow the instructions (`README.html` found in the top-level unzipped folder) to install and run a local, sandbox Xand Network.
## Launch Xpay
* From the `xpay-demo` folder, type `cargo run` to launch `xpay-demo`.  You may run multiple copies of `xpay-demo` (`cargo run &` will allow you to launch a
second instance):
  * On the second instance, click `AliceCo` in the lower-left corner of the UI, click `Current` and change the Xand Address in the drop-down to
  configure one of the apps to represent a second Member.

# Slint Development
Docs: https://slint-ui.com/releases/0.2.0/docs/cpp/language.html
Examples (see "Preview Online" url under each item in README: https://github.com/slint-ui/slint/tree/master/examples
Online Editor (Live) Examples: https://slint-ui.com/snapshots/master/editor/

Install [slint-viewer](https://github.com/slint-ui/slint/tree/master/tools/viewer) with:
```
cargo install slint-viewer
```

And hot-reload the slint UI with:
```
slint-viewer ui/appwindow.slint --auto-reload
```

Definitely recommend VSCode and their 'Slint UI' extension. They have a "Show preview" 

UI structure and impl was mostly copied then modified from: 
- Online editor (https://slint-ui.com/snapshots/master/editor/?load_url=https://raw.githubusercontent.com/slint-ui/slint/master/examples/printerdemo/ui/printerdemo.slint)
- [Source](https://github.com/slint-ui/slint/tree/master/examples/printerdemo)

# Icons

Download SVG icons into `ui/images` folder from here: https://fonts.google.com/icons


## Bugs and TODOs

### Feature ideas
- Add toggle for showing receipts in Activity Log
- Add toggle for showing timestamps per Activity Log
- Format $ amount after submission
- (Refactor) Have `api_doer` only send back `CreateUpdate` for Create, not full `PaymentUpdate`s
- (low-pri) Surface errors
- 



### Bugs
- Still using `dummy_account` for bank account across handlers
- Where is the "loading..." text on Make a Payment?
- Text took a while to show up?

## Feedback 

### Ankit
- Add bank icons
- Modify demo to be cross-bank


### Erin
- Have a "Create Digital Cash" button/page 

### Brad
- (maybe defer) have a title bar above whole app says "xpay demo applet"

- Give a name for the Counterparties. 
  - Use some fun well known entity names.



### Joe

- Highlight of the BankBalance dropping and Xand balance - kind of a pop when the balances change
  - "tweening"


### Jake

- Have various steps outlined before.
  - Disabled circles that show "future" or "current"
  - Set expectations for process for the user and show where in the process they are
- (defer) Make background offwhite (use the brand guideline color)


# Demo feedback

## Erin
- Splitting up into 2 functions is needed. 
  - Create vs Payment
  - As a voiceover, "we split it up like so, but your automation/wallet can be configured" 
  - Put Entity name somewhere on app so it's clear which window is associated with which entity
  - Less detail on Dunder Mifflin's background 
  - When describing the transactions, be more explicit about what is calling what
    - eg The software this member is hosting is communicating with the blockchain network vai its xand node
  - Helpful to mention "Xand gets locked up"
  - State up front that they'll see the balances at the top update in real time.

# Script Outline

- "Core functionality"
- "Xandbox"
- "Balance up top will update in real time as we issue transactions"

- "First, Settling on the xand network"
- "Members may want to hold a Xand balance for ...."
- "Suppose a Member needs to pay a credit card"
- "Redemption is the inverse of the Creation process"
- "While Redemption is pending fulfillment, funds are locked up, can see the Member's available balance has decreased by the amount"

Script: [](https://transparentfinancialsystems-my.sharepoint.com/:w:/g/personal/ankit_tpfs_io/EdD3BqOWM55MsLQRL0mrTQMB-ftQldWGYnyLPACTnHOcUw?e=OY7qmp)
