## Notes

- "Blocking version of reqwest is not meant to work in WASM" - https://github.com/seanmonstar/reqwest/issues/1300#issuecomment-878630534
- CORS issue when running in browser. While MemberAPI "allows all", firefox/chrome gets an error when fetching policy
  - Disable in Firefox with:  https://addons.mozilla.org/en-US/firefox/addon/cors-everywhere/?utm_source=addons.mozilla.org&utm_medium=referral&utm_content=search
  - Seems to be an issue with actix returning 400 for the OPTIONS request

- Possible issues
  - (browser) CORS
  - (wasm) sleep in CheckTxn retry loop impl

  
- Bug in Member API, error shapes on swagger doc aren't accurate. For error on /transactions/claims/send
  it says:

  ```json
  {
    "message": "Some error text to be placed by the specific error that has occurred."
  }
  ```
  but got back
  ```json
  {
    "XandClientError": {
      "message": "while issuing transfer",
      "source": {
        "BadRequest": {
          "message": "\"InsufficientClaims\""
        }
      }
    }
  }
  ```

## TODOs

- Deployment / Auth / Access work

- Tests for MemberApi returning ::NotFound when GET /transaction returning 404
  
- Is there a way to have API routes be built/"joined" at compile time?

- address use of unwrap in local Usd type

- Fix mixing of `XandBalance`, `BankBalance`, `Usd`. 
  - Replace `XandBalance`/`BankBalance` usage with `Usd` in ApiDoer / MemberApi
  - Remove `.unwraps()` used in converting to `Usd`s.

- Rename types
  - `ApiDoer` -> `??`
  - 

- Integ tests for the hand-rolled client?

- (done) Write `State` struct, where `Handler` struct can update
  - `State` always holds Option<Data> you might want
  - `UiHandler` uses `ApiDoer` to kick off (meta) activity, then updates State
  - (behind the scenes / backend )
  - `ApiDoer` breaks down meta-activity and completes sub-tasks using `MemberApiClient`
  - `MemberApiClient` builds and submits requests to Member API

## Bugs
- Member API swagger says accountId is of type number(i32), but API returns a string (resulting in json deserialize error)
- 


## Readme updates:

On linux, need to
```bash
sudo apt install binaryen
```

## PRD

### Access

### Deployment

### Applet

- “Bank Balance” view


- “Digital Cash Balance” view 

- “Make a Payment” button

- “Redeem Digital Cash” button


- “Settings” hamburger menu button. 

### Configurability




