use bank_info::BankInfo;
use jwt_info::JwtInfo;
pub use member_api_details::MemberApiDetails;
pub use member_metadata::MemberMetadata;
use xand_api_details::XandApiDetails;

mod bank_info;
mod jwt_info;
mod member_api_details;
mod member_metadata;
mod xand_api_details;

#[derive(Clone, Debug, Deserialize, Serialize)]
#[serde(rename_all = "kebab-case")]
pub struct EntitiesMetadata {
    pub members: Vec<MemberMetadata>,
}
