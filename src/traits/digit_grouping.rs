#[derive(Clone, Copy, Debug, Eq, Hash, PartialEq, serde::Deserialize, serde::Serialize)]
pub enum DigitGroupingPolicy {
    Common,
}

impl Default for DigitGroupingPolicy {
    fn default() -> Self {
        Self::Common
    }
}
