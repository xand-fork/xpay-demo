//! This module aliases the slint-generated structs and impls adapters concerning them

use crate::backend::domain_state::payment_promise::{CreateUpdate, PaymentUpdate};
use crate::backend::domain_state::redeem_promise::RedeemUpdate;
use crate::backend::domain_state::DomainUiRecipient;
use crate::backend::ui_state::activity_log_builder::ActivityLogBuilder;
use crate::{
    backend::domain_state::api_doer::error::Error, backend::domain_state::data::MemberData, Account, AppWindow,
    ComponentHandle, DigitGroupingPolicy, DigitGroupingsExt, Progress, UiBankAccount, UiRecipient, Usd,
};
use slint::Weak;

type UiMemberData = crate::UiMemberData;

pub struct UiStateHandle {
    app_window: Weak<AppWindow>,
}

impl UiStateHandle {
    pub const fn new(app_window: Weak<AppWindow>) -> Self {
        UiStateHandle { app_window }
    }
    pub fn get_current_bank_account_index(&self) -> i32 {
        let window = self.app_window.unwrap();
        let g = window.global::<crate::GlobalState>();

        g.get_current_bank_account_index()
    }

    pub fn get_current_member_index(&self) -> i32 {
        let window = self.app_window.unwrap();
        let g = window.global::<crate::GlobalState>();

        g.get_current_member_index()
    }

    pub fn initialize_possible_bank_accounts(&self, p0: &[Account]) {
        let window = self.app_window.unwrap();
        let g = window.global::<crate::GlobalState>();

        let ui_bank_accounts: Vec<UiBankAccount> = p0.iter().map(Clone::clone).map(Into::into).collect();
        let ui_bank_accounts = slint::ModelRc::new(slint::VecModel::from(ui_bank_accounts));

        g.set_possible_bank_accounts(ui_bank_accounts);
    }
    pub fn initialize_possible_members(&self, p0: &[MemberData]) {
        let window = self.app_window.unwrap();
        let g = window.global::<crate::GlobalState>();

        let ui_member_data: Vec<UiMemberData> = p0.iter().map(Clone::clone).map(Into::into).collect();
        let ui_possible_members = slint::ModelRc::new(slint::VecModel::from(ui_member_data));

        g.set_possible_members(ui_possible_members);
    }

    pub fn update_bank_bal(&self, p0: &Result<Usd, Error>) {
        let window = self.app_window.unwrap();
        let g = window.global::<crate::GlobalState>();

        let bal: String = match p0 {
            Ok(v) => fmt_usd(v),
            // TODO: If err, indicate on UI somewhere
            Err(_) => "--fetching--".into(),
        };
        g.set_bank_balance(bal.into());
    }

    pub fn clear_bank_balance(&self) {
        let window = self.app_window.unwrap();
        let g = window.global::<crate::GlobalState>();
        g.set_bank_balance("--fetching--".into());
    }

    pub fn clear_xand_balance(&self) {
        let window = self.app_window.unwrap();
        let g = window.global::<crate::GlobalState>();
        g.set_xand_balance("--fetching--".into());
    }

    pub fn update_xand_bal(&self, p0: &Result<Usd, Error>) {
        let window = self.app_window.unwrap();
        let g = window.global::<crate::GlobalState>();

        let bal: String = match p0 {
            Ok(v) => fmt_usd(v),
            // TODO: If err, indicate on UI somewhere
            Err(_) => "--fetching--".into(),
        };
        g.set_xand_balance(bal.into());
    }

    pub fn update_create_status(&self, progress: Vec<PaymentUpdate>) {
        let window = self.app_window.unwrap();
        let g = window.global::<crate::GlobalState>();

        let payment_progress = Progress::from(progress);
        g.set_create_progress(payment_progress);
    }

    pub fn update_payment_status(&self, progress: Vec<PaymentUpdate>) {
        let window = self.app_window.unwrap();
        let g = window.global::<crate::GlobalState>();

        let payment_progress = Progress::from(progress);
        g.set_payment_progress(payment_progress);
    }

    pub fn update_redeem_status(&self, progress: Vec<RedeemUpdate>) {
        let window = self.app_window.unwrap();
        let g = window.global::<crate::GlobalState>();

        let redeem_progress = Progress::from(progress);
        g.set_redeem_progress(redeem_progress);
    }
}

impl Default for UiStateHandle {
    fn default() -> Self {
        let app_window = AppWindow::new();
        let app_window = app_window.as_weak();
        Self { app_window }
    }
}

impl From<Vec<RedeemUpdate>> for Progress {
    fn from(updates: Vec<RedeemUpdate>) -> Self {
        let mut p = Self::default();
        let mut builder = ActivityLogBuilder::new();
        for u in updates {
            match u {
                RedeemUpdate::Submitted(ts) => {
                    builder.header("Redeem Digital Cash");
                    builder.indented(format!(
                        "{} Submitting Redeem Request transaction to distributed ledger...",
                        ts.local_time_str()
                    ));
                },
                RedeemUpdate::Finalized(ts) => {
                    builder.indented(format!("{} Redeem Request accepted.", ts.local_time_str()));
                    builder.indented(format!("{} Waiting for Trust to confirm...", ts.local_time_str()));
                },
                RedeemUpdate::Fulfilled(ts) => {
                    builder.indented(format!("{} Redeem Request fulfilled.", ts.local_time_str()));
                },
            }
        }
        p.activity_log = builder.build().into();
        p
    }
}

impl From<Vec<PaymentUpdate>> for Progress {
    fn from(updates: Vec<PaymentUpdate>) -> Self {
        let mut p = Self::default();
        let mut builder = ActivityLogBuilder::new();
        for u in updates {
            match u {
                PaymentUpdate::CreateUpdate(create_update) => match create_update {
                    CreateUpdate::CreateRequestSubmitted(ts, _txn_receipt) => {
                        builder.header("Create Digital Cash");
                        builder.indented(format!(
                            "{} Submitting Create Request transaction to distributed ledger...",
                            ts.local_time_str()
                        ));
                    },
                    CreateUpdate::CreateRequestAccepted(ts) => {
                        builder.indented(format!("{} Create Request accepted.", ts.local_time_str()));
                    },
                    CreateUpdate::BookTransferSubmitted(ts) => {
                        builder.indented(format!("{} Transferring funds to Trust...", ts.local_time_str()));
                    },
                    CreateUpdate::BookTransferAccepted(ts) => {
                        builder.indented(format!("{} Funds Transfer Successful.", ts.local_time_str()));
                        builder.indented(format!("{} Waiting for Trust to confirm...", ts.local_time_str()));
                    },
                    CreateUpdate::CashConfirmation(ts, _txn) => {
                        builder.indented(format!("{} Digital Cash received.", ts.local_time_str()));
                    },
                },
                PaymentUpdate::PaymentSubmitted(ts, _txn_receipt) => {
                    builder.header("Send Payment");
                    builder.indented(format!(
                        "{} Submitting Payment transaction to distributed ledger...",
                        ts.local_time_str()
                    ));
                },
                PaymentUpdate::PaymentFinalized(ts, _txn) => {
                    builder.indented(format!("{} Payment transaction accepted", ts.local_time_str()));
                    builder.indented(format!("{} Payment settled!", ts.local_time_str()));
                },
            }
        }
        p.activity_log = builder.build().into();
        p
    }
}

impl From<DomainUiRecipient> for UiRecipient {
    fn from(r: DomainUiRecipient) -> Self {
        UiRecipient { address: r.address.into(), friendly_name: r.friendly_name.into() }
    }
}
impl From<MemberData> for UiMemberData {
    fn from(m: MemberData) -> Self {
        Self {
            address: m.address.into(),
            jwt: m.member_api_details.jwt_or_empty().into(),
            url: m.member_api_details.member_api_url.to_string().into(),
        }
    }
}

impl From<Account> for UiBankAccount {
    fn from(a: Account) -> Self {
        Self { account: a.id().to_string().into() }
    }
}

mod activity_log_builder {
    const FOUR_SPACES: &str = "    ";
    const DEFAULT_SEPARATOR: &str = "\n\n";

    pub struct ActivityLogBuilder {
        separator: String,
        buffer: String,
    }

    impl ActivityLogBuilder {
        pub fn new() -> Self {
            ActivityLogBuilder {
                // By default, each entry is separated by 2 new lines
                separator: DEFAULT_SEPARATOR.into(),
                buffer: "".into(),
            }
        }

        pub fn header<S: AsRef<str>>(&mut self, val: S) -> &mut Self {
            self.buffer.push_str(val.as_ref());
            self.buffer.push_str(&self.separator);
            self
        }

        pub fn indented<S: AsRef<str>>(&mut self, val: S) -> &mut Self {
            let text = val.as_ref();

            let text = text.split('\n').collect::<Vec<&str>>().join(&format!("\n{}", FOUR_SPACES));
            let indented_text = format!("{}{}", FOUR_SPACES, text);
            self.buffer.push_str(&indented_text);
            self.buffer.push_str(&self.separator);

            self
        }

        #[allow(clippy::missing_const_for_fn)]
        pub fn build(self) -> String {
            if self.buffer.is_empty() {
                return "loading...".into();
            }
            self.buffer
        }
    }

    #[cfg(test)]
    mod tests {
        use super::*;

        #[test]
        fn header__adds_text_with_newline() {
            // Given
            let mut builder = ActivityLogBuilder::new();

            // When
            builder.header("MyHeader");

            // and then
            let result = builder.build();

            // Then
            assert_eq!(result, format!("MyHeader{DEFAULT_SEPARATOR}"));
        }

        #[test]
        fn indented__adds_indented_text_with_newline() {
            // Given
            let mut builder = ActivityLogBuilder::new();

            // When
            builder.indented("IndentThisText");

            // and then
            let result = builder.build();

            // Then
            assert_eq!(result, format!("{FOUR_SPACES}IndentThisText{DEFAULT_SEPARATOR}"));
        }

        #[test]
        fn indented__multiline_str_indents_all_lines() {
            // Given
            let mut builder = ActivityLogBuilder::new();

            // When
            builder.indented("IndentedText\nOverMultiple\nLines");

            // and then
            let result = builder.build();

            // Then
            let expected = format!(
                "{FOUR_SPACES}IndentedText\
                \n{FOUR_SPACES}OverMultiple\
                \n{FOUR_SPACES}Lines{DEFAULT_SEPARATOR}"
            );
            assert_eq!(result, expected);
        }

        #[test]
        fn header_then_indented__header_gets_own_line() {
            // Given
            let mut builder = ActivityLogBuilder::new();

            // When
            builder.header("Header");
            builder.indented("IndentedText");

            // and then
            let result = builder.build();

            // Then
            let expected = format!("Header{DEFAULT_SEPARATOR}{FOUR_SPACES}IndentedText{DEFAULT_SEPARATOR}");
            assert_eq!(result, expected);
        }

        #[test]
        fn build__create_digital_cash_sample_log_formats_correctly() {
            // Given
            let mut builder = ActivityLogBuilder::new();

            // When
            builder.header("Create");
            builder.indented("Submitting...");
            builder.indented("Accepted.");
            let result = builder.build();

            // Then
            let expected = "Create\
            \n\n    Submitting...\
            \n\n    Accepted.\
            \n\n";
            assert_eq!(result, expected)
        }
    }
}
fn fmt_usd(usd: &Usd) -> String {
    let number = usd.to_major_units().group_digits(DigitGroupingPolicy::Common);
    format!("$ {}", number)
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn fmt_usd__amount_with_decimals_renders_correctly() {
        // Given
        let d = Usd::try_from_str("1200.01").unwrap();

        // When
        let result = fmt_usd(&d);

        // Then
        assert_eq!(result, "$ 1,200.01")
    }

    #[test]
    fn fmt_usd__whole_amount_renders_decimals_as_zero() {
        // Given
        let d = Usd::try_from_str("1200").unwrap();

        // When
        let result = fmt_usd(&d);

        // Then
        assert_eq!(result, "$ 1,200.00")
    }
}
