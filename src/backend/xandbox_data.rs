use crate::backend::xandbox_data::entities_metadata::MemberMetadata;
use entities_metadata::EntitiesMetadata;

pub mod entities_metadata;

const ENTITIES_METADATA_YAML_STR: &str = include_str!("xandbox_data/entities-metadata.yaml");

pub struct EntitiesMetadataLoader {}

impl EntitiesMetadataLoader {
    pub fn load_permissioned_static() -> Vec<MemberMetadata> {
        Self::parse(ENTITIES_METADATA_YAML_STR).unwrap()
    }

    pub fn parse(metadata_yaml: &str) -> Result<Vec<MemberMetadata>, String> {
        let metadata = serde_yaml::from_str(metadata_yaml).map_err(|err| err.to_string())?;
        Ok(Self::load_permissioned_from(metadata))
    }

    fn load_permissioned_from(metadata: EntitiesMetadata) -> Vec<MemberMetadata> {
        metadata.members.into_iter().filter(|m| m.initially_permissioned).collect()
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn load__can_parse_entities_metadata_yaml_file() {
        // When
        let members = EntitiesMetadataLoader::load_permissioned_static();

        // Then - smoke test the 0th member
        let zeroth_member = members.get(0).unwrap();
        assert!(zeroth_member.initially_permissioned);
        assert!(zeroth_member.member_api_details.auth.is_some());
    }

    #[test]
    fn load_permissioned_members__all_resulting_members_are_permissioned() {
        // When
        let members = EntitiesMetadataLoader::load_permissioned_static();

        // Then
        assert!(members.iter().all(|m| m.initially_permissioned))
    }
}
