pub use spawn::spawn_async;
mod spawn {
    use poll_promise::Promise;

    #[cfg(target_arch = "wasm32")]
    pub fn spawn_async<T: Send>(future: impl std::future::Future<Output = T> + 'static) -> Promise<T> {
        let (sender, promise) = Promise::new();
        wasm_bindgen_futures::spawn_local(async move { sender.send(future.await) });
        promise
    }

    #[cfg(not(target_arch = "wasm32"))]
    pub fn spawn_async<T: Send>(future: impl std::future::Future<Output = T> + 'static + Send) -> Promise<T> {
        Promise::spawn_thread("fut_resolver", || {
            let rt = tokio::runtime::Builder::new_current_thread()
                .enable_all()
                .build()
                .expect("Could not build tokio runtime");
            rt.block_on(future)
        })
    }
}
pub use sleep::sleep;

#[allow(unused_variables)]
mod sleep {

    use std::time::Duration;

    #[cfg(target_arch = "wasm32")]
    mod web {
        use std::time::Duration;
        use wasm_bindgen::JsValue;

        // From: https://www.reddit.com/r/rust/comments/cpxjlw/wasmasync_discoveris_about_sleepawait_via/
        pub async fn timer(duration: Duration) -> Result<(), JsValue> {
            use js_sys::Promise;

            use wasm_bindgen_futures::JsFuture;
            use web_sys::window;

            let ms = duration.as_millis().try_into().unwrap();
            let promise = Promise::new(&mut |yes, _| {
                let win = window().unwrap();

                win.set_timeout_with_callback_and_timeout_and_arguments_0(&yes, ms).unwrap();
            });
            let js_fut = JsFuture::from(promise);
            js_fut.await?;
            Ok(())
        }
    }

    pub async fn sleep(duration: Duration) {
        #[cfg(target_arch = "wasm32")]
        web::timer(duration).await.unwrap();

        #[cfg(not(target_arch = "wasm32"))]
        tokio::time::sleep(duration).await;
    }
}

pub mod time {
    use chrono::{DateTime, Local, TimeZone, Utc};
    use std::fmt::Display;

    // See escape sequences here: https://docs.rs/chrono/latest/chrono/format/strftime/index.html
    const TIME_DISPLAY_FMT: &str = "%l:%M:%S %P";

    #[derive(Clone)]
    pub struct Timestamp(DateTime<Utc>);

    impl Timestamp {
        pub fn now() -> Self {
            Self(chrono::Utc::now())
        }

        pub fn local_time_str(&self) -> String {
            self.time_str_for_tz(Local)
        }

        pub fn time_str_for_tz<TZ: TimeZone>(&self, tz: TZ) -> String
        where
            <TZ as TimeZone>::Offset: Display,
        {
            let tz_time = self.0.with_timezone(&tz);

            tz_time.format(TIME_DISPLAY_FMT).to_string()
        }
    }

    #[cfg(test)]
    mod tests {
        use super::*;
        use chrono::TimeZone;
        use chrono_tz::US::Arizona;

        #[test]
        fn time_str__prints_local_ts() {
            // Given
            // `2014-07-08T09:00:0Z`
            let dt = Utc.ymd(2020, 6, 1).and_hms(9, 0, 0);
            let t = Timestamp(dt);
            let local_rep = dt.with_timezone(&Local);
            let local_time_str = local_rep.format(TIME_DISPLAY_FMT).to_string();

            // When
            let t_str = t.local_time_str();

            // Then

            assert_eq!(t_str, local_time_str);
        }

        #[test]
        fn tz_time_str__can_pass_timezone() {
            // Given
            // `2014-07-08T09:00:0Z`
            let dt = Utc.ymd(2020, 6, 1).and_hms(9, 0, 0);
            let t = Timestamp(dt);

            // When
            // Testing with a Timezone that does not observe DST
            // https://docs.microsoft.com/en-us/outlook/troubleshoot/calendaring/time-zones-that-do-not-observe-daylight-saving-time#time-zones-that-do-not-observe-daylight-saving-time-1
            let t_str = t.time_str_for_tz(Arizona);

            // Then
            assert_eq!(t_str, " 2:00:00 am");
        }
    }
}
