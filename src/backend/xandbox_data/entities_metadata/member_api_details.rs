use super::JwtInfo;
use url::Url;

#[derive(Clone, Debug, Deserialize, Serialize)]
#[serde(rename_all = "kebab-case")]
pub struct MemberApiDetails {
    pub member_api_url: Url,
    pub auth: Option<JwtInfo>,
}

impl MemberApiDetails {
    pub fn jwt_or_empty(&self) -> String {
        self.auth.as_ref().map_or("".into(), |jwt_info| jwt_info.token.0.to_string())
    }
}
