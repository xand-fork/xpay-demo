This dir deploys the `xpay-demo` applet behind [nginx + oauth2_proxy].

It targets the `tpfs-apps` cluster located here: https://console.cloud.google.com/kubernetes/clusters/details/us-central1-b/tpfs-apps/details?authuser=1&project=xand-dev

Get the `tpfs-apps` cluster credentials with:
```
gcloud container clusters get-credentials tpfs-apps --zone us-central1-b --project xand-dev
```

Apply everything with:
```bash
kubectl apply -k .
```

Everything deploys to the `xpay-demo` namespace. 

## TODOs:
- Note down where to fetch the AD app details (client id, client secret, oidc issuer url)
- Note down the manual mod needed for the AD app
- 
