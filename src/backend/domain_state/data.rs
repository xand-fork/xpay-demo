use crate::backend::clients::models;
use std::fmt::{Display, Formatter};

pub use member_data::MemberData;

mod member_data {
    use crate::backend::xandbox_data::entities_metadata::{MemberApiDetails, MemberMetadata};

    #[derive(Clone)]
    pub struct MemberData {
        pub address: String,
        pub member_api_details: MemberApiDetails,
    }

    impl From<MemberMetadata> for MemberData {
        fn from(m: MemberMetadata) -> Self {
            Self { address: m.address, member_api_details: m.member_api_details }
        }
    }

    #[cfg(test)]
    impl MemberData {
        pub fn test() -> Self {
            use url::Url;

            let member_api_url = Url::parse("http://localhost:3000").unwrap();
            let address = "5DesW2g9miJMahngjaEVHap6kBMiDH33CdgndL5d75GkY7oz".to_string();
            Self { address, member_api_details: MemberApiDetails { member_api_url, auth: None } }
        }

        pub fn test_with(address: &str) -> Self {
            let mut member = Self::test();
            member.address = address.to_string();
            member
        }
    }
}
#[derive(Clone)]
pub struct BankAccountAggregate {
    pub account: Account,
    pub bank_balance: Usd,
}

impl Display for BankAccountAggregate {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        let summary = format!("{}\n{:?}", self.account, self.bank_balance);
        write!(f, "{}", summary)
    }
}
#[derive(Clone)]
pub struct Account(models::BankAccount);

impl Account {
    pub const fn id(&self) -> i32 {
        self.0.id
    }

    pub fn dummy_account() -> Self {
        Self(models::BankAccount {
            id: 1,
            bank_id: 1,
            bank_name: "local_test".to_string(),
            short_name: "local_test".to_string(),
            routing_number: "111".to_string(),
            masked_account_number: "222".to_string(),
        })
    }
}

impl Display for Account {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        let acct_str = format!(
            "id: {}, routing: {}, account_number: {}",
            self.0.id, self.0.routing_number, self.0.masked_account_number
        );
        write!(f, "{}", acct_str)
    }
}

pub use address::Address;

mod address {
    #[derive(Default, Debug, Clone, Eq, PartialEq)]
    pub struct Address(pub String);

    impl Address {
        // TODO: Rename this to `fn human_readable_addres()`? Or something else indicating its the
        // representation required by the member API
        pub fn base_58_str(&self) -> String {
            self.0.clone()
        }
    }

    #[cfg(test)]
    impl Address {
        pub fn test() -> Self {
            "5GedXksPiCaXqv114r2NPQQbgirgA9X8pyRM1AHZ42bDGomj".try_into().unwrap()
        }
    }

    impl TryFrom<&str> for Address {
        type Error = ();

        fn try_from(value: &str) -> Result<Self, Self::Error> {
            // TODO: Actually parse and check is valid address?
            Ok(Self(value.to_string()))
        }
    }
}

pub use usd::Usd;

#[allow(clippy::unwrap_used)]
pub mod usd {
    use num_traits::ToPrimitive;
    use std::ops::Sub;
    use xand_money::{Decimal, Money};

    pub use error::Error;
    mod error {
        use thiserror::Error;

        #[derive(Debug, Error)]
        pub enum Error {
            #[error(transparent)]
            Conversion(#[from] xand_money::MoneyError),
        }
    }
    #[derive(Copy, Clone, Debug, Eq, PartialEq, PartialOrd)]
    pub struct Usd(xand_money::Usd);

    impl Usd {
        pub fn sub(&self, rhs: Self) -> Self {
            let sub_result = self.0.into_minor_units().sub(rhs.0.into_minor_units());

            // TODO: Is there a way to do math with Usd? Or, is there  way to go from Decimal to Usd infallibly? Can Usd
            // be negative?
            let usd = xand_money::Usd::from_u64_minor_units(sub_result.to_u64().unwrap()).unwrap();
            Self(usd)
        }

        pub fn to_minor_units_i32(self) -> i32 {
            self.0.into_minor_units().to_i32().unwrap()
        }

        pub fn to_major_units(self) -> Decimal {
            let mut major_units = self.0.into_major_units();
            major_units.rescale(2);
            major_units
        }

        pub fn try_from_i32(amt: i32) -> Result<Self, error::Error> {
            let usd = xand_money::Usd::from_i64_minor_units(amt)?;
            Ok(Self(usd))
        }

        pub fn try_from_u64(amt: u64) -> Result<Self, error::Error> {
            let usd = xand_money::Usd::from_u64_minor_units(amt)?;
            Ok(Self(usd))
        }

        pub fn try_from_str<S: AsRef<str>>(amt: S) -> Result<Self, error::Error> {
            let amt = amt.as_ref();
            let amt = amt.replace(',', "");
            dbg!(&amt);
            let inner = xand_money::Usd::from_str(amt)?;
            Ok(Self(inner))
        }

        #[cfg(test)]
        pub fn test_from_u64(amt: u64) -> Self {
            let usd = xand_money::Usd::from_u64_minor_units(amt).unwrap();
            Self(usd)
        }
    }
}
#[derive(Default, Debug, Clone)]
pub struct PaymentReceipt {}

impl PaymentReceipt {
    pub const fn new() -> Self {
        Self {}
    }
}

#[derive(Default, Debug, Clone)]
pub struct RedeemReceipt {}

impl RedeemReceipt {
    pub const fn new() -> Self {
        Self {}
    }
}

#[derive(Default, Debug, Clone)]
pub struct TransactionReceipt {
    txn: models::Receipt,
}

pub type Nonce = String;
impl TransactionReceipt {
    pub const fn new(txn: models::Receipt) -> Self {
        Self { txn }
    }

    pub fn nonce(&self) -> Option<Nonce> {
        self.txn.nonce.clone()
    }

    pub fn txn_id(&self) -> String {
        self.txn.transaction_id.clone()
    }
}

#[cfg(test)]
impl TransactionReceipt {
    pub fn test() -> Self {
        use crate::backend::domain_state::data::models::Receipt;
        Self { txn: Receipt::default() }
    }

    pub fn test_with_nonce<S: AsRef<str>>(id: S, nonce: S) -> Self {
        Self {
            txn: models::Receipt { transaction_id: id.as_ref().to_string(), nonce: Some(nonce.as_ref().to_string()) },
        }
    }
}

pub type CreateReceipt = BookTransferReceipt;

#[derive(Default, Debug, Clone)]
pub struct BookTransferReceipt(TransactionReceipt, models::TransferReceipt);

impl BookTransferReceipt {
    pub const fn new(create_request: TransactionReceipt, book_transfer: models::TransferReceipt) -> Self {
        Self(create_request, book_transfer)
    }

    pub fn create_txn_receipt(&self) -> TransactionReceipt {
        self.0.clone()
    }
}

#[cfg(test)]
impl BookTransferReceipt {
    pub fn test_with(amt: i32) -> Self {
        let txn_receipt = TransactionReceipt::test();
        Self::new(txn_receipt, models::TransferReceipt { amount_in_minor_unit: Some(amt) })
    }
}
